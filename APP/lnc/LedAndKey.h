#ifndef _LEDANDKEY_H_
#define _LEDANDKEY_H_

#include "Common.h"
#include "BaseLib.h"


typedef enum
{
  eLEDANDKEY_TEST_IDLE = 0x00,        //闲置状态
  eLEDANDKEY_TEST_WAIT,               //等待
  eLEDANDKEY_TEST_LEDANDKEY,            //显示状态
  eLEDANDKEY_END,                     //结束状态

} eLedAndKeyStateTest_TYPE;

typedef enum
{
  eLEDANDKEY_IDLE = 0x00,        //闲置状态
  eLEDANDKEY_PERIOD_WAIT, //周期等待
  eLEDANDKEY_INITIALIZE_STATE,//初始化状态
  eLEDANDKEY_WAIT_STATUS , //等待复位
  eLEDANDKEY_SET_WORKMODE , //设置工作模式
  eLEDANDKEY_STARTWORT,   //开始工作
  eLEDANDKEY_START_MEASUREMENT , //PMSA开始读数
  eLEDANDKEY_WAIT_MEASUREMENT ,  //等待读数
  eLEDANDKEY_READ_MEASUREMENT ,  //PMSA采样读数

} eLedAndKeyState_TYPE;

typedef struct 
{
    eLedAndKeyState_TYPE eRunState; //状态机
    
    eLedAndKeyStateTest_TYPE eRunStateTest; //测试状态机

    uint32_t u32TestWait;  //测试等待时间

    uint8_t u8TestKeyValue;//测试按键值

} eLedAndKey_Typedef;
/*********************************************************************/
/*********************************************************************/
//extern sTM16XX_Typedef sTM16XX;
extern eLedAndKey_Typedef LedAndKey; 
/*********************************************************************/

/*********************************************************************
 * @fn      LedAndKey_Tick1000s
 * @brief   按键延时（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void LedAndKey_Tick1000s(eLedAndKey_Typedef *pLedAndKey);
/*********************************************************************
 * @fn      LedAndKey_OperationTask_Test
 * @brief   按键测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void LedAndKey_OperationTask_Test(eLedAndKey_Typedef *pLedAndKey); 







#endif
