#ifndef _IOT_MODULE_H_
#define _IOT_MODULE_H_

#include "Common.h"
#include "BaseLib.h"



/*********************************************************************/

/*********************************************************************/


typedef enum
{
  eIOTMODULE_TEST_IDLE = 0x00,        //闲置状态
  eIOTMODULE_TEST_WAIT,               //等待
  eIOTMODULE_TEST_IOTMODULE,            //显示状态
  eIOTMODULE_END,                     //结束状态

} eIOTMODULEStateTest_TYPE;

typedef enum
{
  eIOTMODULE_IDLE = 0x00,        //闲置状态
  eIOTMODULE_PERIOD_WAIT, //周期等待
  eIOTMODULE_INITIALIZE_STATE,//初始化状态
  eIOTMODULE_WAIT_STATUS , //等待复位
  eIOTMODULE_SET_WORKMODE , //设置工作模式
  eIOTMODULE_STARTWORT,   //开始工作
  eIOTMODULE_START_MEASUREMENT , //PMSA开始读数
  eIOTMODULE_WAIT_MEASUREMENT ,  //等待读数
  eIOTMODULE_READ_MEASUREMENT ,  //PMSA采样读数

} eIOTMODULEState_TYPE;


typedef struct 
{
    eIOTMODULEState_TYPE eRunState; //状态机
    eIOTMODULEStateTest_TYPE eRunStateTest; //测试状态机

    uint32_t u32TestWait;  //测试等待时间

   

} IOTModule_Typedef;
/*********************************************************************/
//extern sTM16XX_Typedef sTM16XX;
extern IOTModule_Typedef IOTModule; 
/*********************************************************************/

/*********************************************************************
 * @fn      IOTModule_Reset
 * @brief   IOT模块复位
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_Reset(void);

/*********************************************************************
 * @fn      IOTModule_Tick1000s
 * @brief   IOT模块延时
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_Tick1000s(IOTModule_Typedef *pIOTModule);
/*********************************************************************
 * @fn      IOTModule_OperationTask_Test
 * @brief   LED显示测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_OperationTask_Test(IOTModule_Typedef *pIOTModule) ;



#endif




