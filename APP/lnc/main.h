#ifndef __MAIN_H__
#define __MAIN_H__


#include "BSP_Include.h"

typedef struct
{
    //uint8_t u8Timer_1ms;
    uint32_t u32ms;
    uint8_t u8Cnt_1ms;
    uint8_t u8Cnt_10ms;
    uint8_t u8Cnt_100ms;
    uint8_t u8Timer_100us;
    uint8_t u8Timer_1ms;
    uint8_t u8Timer_10ms;
    uint8_t u8Timer_100ms;
    uint8_t u8Timer_1s;
    uint8_t _u8Second180;

}Timer_Flag_Typedef;

/*********************************************************************
 * @fn      TIM0_Interrupt_1ms
 * @brief   1ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void TIM0_Interrupt_1ms(void);
/*********************************************************************
 * @fn      TimeDelay
 * @brief   延时函数
 * @param   无参数
 * @return  none
*********************************************************************/
void TimeDelay(void);
/*********************************************************************
 * @fn      SystemTimeTick_10ms
 * @brief   10ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_10ms(void);
/*********************************************************************
 * @fn      SystemTimeTick_100ms
 * @brief   100ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_100ms(void);
/*********************************************************************
 * @fn      SystemTimeTick_1000ms
 * @brief   1s定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_1000ms(void);


#endif
