#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "Common.h"
#include "BaseLib.h"

/*********************************************************************/
#define LED_LINE_NUM            (8)

#define LED_ALL_LIGHT           (0xFFFF) //全亮
#define LED_ALL_DARK            (0x0000) //全暗
/*********************************************************************/


typedef enum
{
  eDISPLAY_TEST_IDLE = 0x00,        //闲置状态
  eDISPLAY_TEST_WAIT,               //等待
  eDISPLAY_TEST_DISPLAY,            //显示状态
  eDISPLAY_END,                     //结束状态

} eDisplayStateTest_TYPE;

typedef enum
{
  eDISPLAY_IDLE = 0x00,        //闲置状态
  eDISPLAY_PERIOD_WAIT, //周期等待
  eDISPLAY_INITIALIZE_STATE,//初始化状态
  eDISPLAY_WAIT_STATUS , //等待复位
  eDISPLAY_SET_WORKMODE , //设置工作模式
  eDISPLAY_STARTWORT,   //开始工作
  eDISPLAY_START_MEASUREMENT , //PMSA开始读数
  eDISPLAY_WAIT_MEASUREMENT ,  //等待读数
  eDISPLAY_READ_MEASUREMENT ,  //PMSA采样读数

} eDisplayState_TYPE;


typedef struct 
{
    eDisplayState_TYPE eRunState; //状态机
    
    eDisplayStateTest_TYPE eRunStateTest; //测试状态机

    uint32_t u32TestWait;  //测试等待时间

    sTM16XX_Typedef sTM16XX;

} Display_Typedef;
/*********************************************************************/
//extern sTM16XX_Typedef sTM16XX;
extern Display_Typedef Display; 
/*********************************************************************/




/*********************************************************************
 * @fn      BSP_TM16XXInterfaceInit
 * @brief   TM16XX初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void BSP_TM16XXInterfaceInit(void);

/*********************************************************************
 * @fn      Display_Tick1000s
 * @brief   显示延时器（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void Display_Tick1000s(Display_Typedef *pDisplay);

/*********************************************************************
 * @fn      DISPLAY_OperationTask_Test
 * @brief   LED显示测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void DISPLAY_OperationTask_Test(Display_Typedef *pDisplay); 

#endif
