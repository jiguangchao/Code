#ifndef _REALTIMECLOCK_H_
#define _REALTIMECLOCK_H_

#include "Common.h"
#include "BaseLib.h"


typedef enum
{
  eRealTimeClock_TEST_IDLE = 0x00,        //闲置状态
  eRealTimeClock_TEST_WAIT,               //等待
  eRealTimeClock_TEST_WRITECOM,           //写入命令
  eRealTimeClock_TEST_READ_WAIT,          //读等待
  eRealTimeClock_TEST_READ,               //读数据
  eRealTimeClock_END,                     //结束状态

} eRealTimeClockStateTest_TYPE;

typedef enum
{
  eRealTimeClock_IDLE = 0x00,        //闲置状态
  eRealTimeClock_PERIOD_WAIT, //周期等待
  eRealTimeClock_INITIALIZE_STATE,//初始化状态
  eRealTimeClock_WAIT_STATUS , //等待复位
  eRealTimeClock_SET_WORKMODE , //设置工作模式
  eRealTimeClock_STARTWORT,   //开始工作
  eRealTimeClock_START_MEASUREMENT , //PMSA开始读数
  eRealTimeClock_WAIT_MEASUREMENT ,  //等待读数
  eRealTimeClock_READ_MEASUREMENT ,  //PMSA采样读数

} eRealTimeClockState_TYPE;

typedef struct 
{
    eRealTimeClockState_TYPE eRunState; //状态机
    eRealTimeClockStateTest_TYPE eRunStateTest; //测试状态机

    uint32_t u32TestWait;  //测试等待时间


    uint8_t  u8ReadRegAdd; //读寄存器地址

    uint8_t  u8Second; //秒
    uint8_t  u8Minute; //分
    uint8_t  u8Hour;   //时
    uint8_t  u8Date;   //日
    uint8_t  u8Week;   //星期
    uint8_t  u8Month;  //月
    uint8_t  u8Year;   //年


    
    sAiP8563_Typedef sAiP8563;
    //uint8_t u8TestKeyValue;//测试按键值

} eRealTimeClock_Typedef;
/*********************************************************************/
/*********************************************************************/
//extern sTM16XX_Typedef sTM16XX;
extern eRealTimeClock_Typedef RealTimeClock; 
/*********************************************************************/


/*********************************************************************
 * @fn      RealTimeClock_Tick1000s
 * @brief   实时时钟延时（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void RealTimeClock_Tick1000s(eRealTimeClock_Typedef *pRealTimeClock);
/*********************************************************************
 * @fn      BSP_AiP8563InterfaceInit
 * @brief   时钟芯片初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void BSP_AiP8563InterfaceInit(void);
/*********************************************************************
 * @fn      AiP8563_InterfaceInit
 * @brief   时钟芯片初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void AiP8563_OperationTask_Test(eRealTimeClock_Typedef *pRealTimeClock);





#endif
