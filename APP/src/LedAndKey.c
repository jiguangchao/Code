#include "Common.h"
#include "BaseLib.h"
#include "LedAndKey.h"


eLedAndKey_Typedef LedAndKey; 

/*********************************************************************
 * @fn      LedAndKey_Tick1000s
 * @brief   按键延时（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void LedAndKey_Tick1000s(eLedAndKey_Typedef *pLedAndKey)
{

}


/*********************************************************************
 * @fn      LedAndKey_OperationTask_Test
 * @brief   按键测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void LedAndKey_OperationTask_Test(eLedAndKey_Typedef *pLedAndKey) 
{
    switch ( pLedAndKey->eRunStateTest )
    {
    case eLEDANDKEY_TEST_IDLE  :

        pLedAndKey->eRunStateTest = eLEDANDKEY_TEST_WAIT;
        break;

    case eLEDANDKEY_TEST_WAIT      :

 
        pLedAndKey->eRunStateTest = eLEDANDKEY_TEST_LEDANDKEY;      
        break;

    case eLEDANDKEY_TEST_LEDANDKEY  :

        if (0x00 !=  BSP_READ_KEY1()  )
        {
            GPIOB_InverseBits(KEY_LED5_PIN);
        }
        if (0x00 !=  BSP_READ_KEY2()  )
        {
            GPIOB_InverseBits(KEY_LED6_PIN);
        }     
        if (0x00 !=  BSP_READ_KEY3()  )
        {
            GPIOB_InverseBits(KEY_LED4_PIN);
        }          
        if (0x00 !=  BSP_READ_KEY4()  )
        {
            GPIOB_InverseBits(KEY_LED3_PIN);
        }       
        if (0x00 !=  BSP_READ_KEY5()  )
        {
            GPIOB_InverseBits(KEY_LED1_PIN);
        }          
        if (0x00 !=  BSP_READ_KEY6()  )
        {
            GPIOB_InverseBits(KEY_LED2_PIN);
        } 


        break;

    case eLEDANDKEY_END             :

        break;
    
    default:
        break;
    }
}

