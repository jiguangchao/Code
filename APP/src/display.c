
#include "Common.h"
#include "BaseLib.h"
#include "display.h"

uint8_t              u8TM16XXLedBuff[LED_LINE_NUM];
uint16_t             u16TM16XXLedBuff[LED_LINE_NUM];
//sTM16XX_Typedef      sTM1629;

Display_Typedef      Display; 
/*********************************************************************
 * @fn      BSP_TM16XXInterfaceInit
 * @brief   TM16XX初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void BSP_TM16XXInterfaceInit(void)
{
    Display.sTM16XX.HWPort.pSetStbHigh    = BSP_TM16XX_SetSTBHigh;
    Display.sTM16XX.HWPort.pSetStbLow     = BSP_TM16XX_SetSTBLow;
    
    Display.sTM16XX.HWPort.pSetClkHigh    = BSP_TM16XX_SetClkHigh;
    Display.sTM16XX.HWPort.pSetClkLow     = BSP_TM16XX_SetClkLow;
    
    Display.sTM16XX.HWPort.pSetDataInput  = BSP_TM16XX_SetDataInput;
    Display.sTM16XX.HWPort.pSetDataOutput = BSP_TM16XX_SetDataOut;
    Display.sTM16XX.HWPort.pGetDataGpio   = BSP_TM16XX_GetDataGpio;
    Display.sTM16XX.HWPort.pSetDataLow    = BSP_TM16XX_SetDataLow;
    Display.sTM16XX.HWPort.pSetDataHigh   = BSP_TM16XX_SetDataHigh;
    
    Display.sTM16XX.HWPort.pWait          = mDelayuS;
    
    //Display.sTM16XX.pu8Button             = u8TM16XXKey;
    Display.sTM16XX.pu8Led                = u8TM16XXLedBuff;
    Display.sTM16XX.pu16Led               = u16TM16XXLedBuff;
}


/*********************************************************************
 * @fn      Display_Tick1000s
 * @brief   显示延时器（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void Display_Tick1000s(Display_Typedef *pDisplay)
{

    if (pDisplay->u32TestWait)
    {
        pDisplay->u32TestWait--;
    }

    
}


/*********************************************************************
 * @fn      DISPLAY_OperationTask_Test
 * @brief   LED显示测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void DISPLAY_OperationTask_Test(Display_Typedef *pDisplay) 
{

    switch (pDisplay->eRunStateTest)
    {
    case eDISPLAY_TEST_IDLE :     //闲置

        pDisplay->u32TestWait   = 5;
        pDisplay->eRunStateTest = eDISPLAY_TEST_WAIT;
        break;

    case eDISPLAY_TEST_WAIT:               //等待

        if (pDisplay->u32TestWait != 0)
        {
            break;
        }
        pDisplay->eRunStateTest = eDISPLAY_TEST_DISPLAY;
        break;

    case eDISPLAY_TEST_DISPLAY:            //显示

        
        pDisplay->sTM16XX.pu16Led[0] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[1] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[2] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[3] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[4] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[5] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[6] = LED_ALL_LIGHT;
        pDisplay->sTM16XX.pu16Led[7] = LED_ALL_LIGHT;
        
        /*
        pDisplay->sTM16XX.pu8Led[0] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[1] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[2] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[3] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[4] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[5] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[6] = LED_ALL_DARK;
        pDisplay->sTM16XX.pu8Led[7] = LED_ALL_DARK;
        */
        TM16XX_LEDControl(&pDisplay->sTM16XX, TM16XX_CMD_DISPLAY_ON);

        pDisplay->eRunStateTest = eDISPLAY_END;
        break;

    case eDISPLAY_END:                     //结束

        pDisplay->eRunStateTest = eDISPLAY_TEST_IDLE;
        break;

    default:
        break;
    }


}



/*********************************************************************
 * @fn      DISPLAY_OperationTask
 * @brief   LED显示状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void DISPLAY_OperationTask(Display_Typedef *pDisplay) 
{




}
