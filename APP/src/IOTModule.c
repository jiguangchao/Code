#include "Common.h"
#include "BaseLib.h"
#include "IOTModule.h"



IOTModule_Typedef   IOTModule; 

/*********************************************************************
 * @fn      IOTModule_Reset
 * @brief   IOT模块复位
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_Reset(void)
{
    GPIOB_ResetBits(WBR1D_U0_NRST_PIN);
    mDelayuS(1000);
    GPIOB_SetBits(WBR1D_U0_NRST_PIN);
    mDelayuS(1000);
    GPIOB_ResetBits(WBR1D_U0_NRST_PIN);
    mDelayuS(1000);
    GPIOB_SetBits(WBR1D_U0_NRST_PIN);

}


/*********************************************************************
 * @fn      IOTModule_Tick1000s
 * @brief   IOT模块延时
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_Tick1000s(IOTModule_Typedef *pIOTModule)
{

    if (pIOTModule->u32TestWait)
    {
        pIOTModule->u32TestWait--;
    }

}


/*********************************************************************
 * @fn      IOTModule_OperationTask_Test
 * @brief   LED显示测试状态机
 * @param   无参数
 * @return  none
*********************************************************************/ 
void IOTModule_OperationTask_Test(IOTModule_Typedef *pIOTModule) 
{

    switch (pIOTModule->eRunStateTest)
    {
    case eIOTMODULE_TEST_IDLE :     //闲置

        pIOTModule->u32TestWait   = 1;
        pIOTModule->eRunStateTest = eIOTMODULE_TEST_WAIT;
        break;

    case eIOTMODULE_TEST_WAIT:               //等待

        if (pIOTModule->u32TestWait != 0)
        {
            break;
        }
        pIOTModule->eRunStateTest = eIOTMODULE_TEST_IOTMODULE;
        break;

    case eIOTMODULE_TEST_IOTMODULE:

        
        IOTModule_Reset();

        pIOTModule->eRunStateTest = eIOTMODULE_END;
        break;

    case eIOTMODULE_END:                     //结束

        //pIOTModule->eRunStateTest = eIOTModule_TEST_IDLE;
        break;

    default:
        break;
    }


}



