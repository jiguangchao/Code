/********************************** (C) COPYRIGHT *******************************
 * File Name          : Main.c
 * Author             : WCH
 * Version            : V1.0
 * Date               : 2020/08/06
 * Description        : 串口1收发演示
 * Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
 * SPDX-License-Identifier: Apache-2.0
 *******************************************************************************/

//#include "CH58x_common.h"
#include "Common.h"
#include "BaseLib.h"


#include "display.h"
#include "LedAndKey.h"
#include "RealTimeClock.h"
#include "IOTModule.h"

#include "main.h"

/*============================ MACROS ========================================*/
/*============================ MACROFIED FUNCTIONS ===========================*/
/*============================ TYPES =========================================*/

/*============================ GLOBAL VARIABLES ==============================*/
Timer_Flag_Typedef TimerFlag;
/*============================ LOCAL VARIABLES ===============================*/

/*============================ PROTOTYPES ====================================*/

/*============================ IMPLEMENTATION ================================*/


uint8_t UART1_TxBuff[64];
uint8_t UART1_RxBuff[64];
uint8_t UART1_trigB;
uint16_t u16crc;



uint8_t UART3_TxBuff[64];
uint8_t UART3_RxBuff[64];
uint8_t UART3_trigB;

/* Private variables ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/



/*********************************************************************
 * @fn      WBR1D_InitialValue
 * @brief   WBR1D串口初始化参数
 * @param   无参数
 * @return  none
*********************************************************************/
void WBR1D_InitialValue(void)
{
    /* Modbus */
    WBR1D_UART.Frame.pu8PhyAddress           = NULL;
    WBR1D_UART.Frame.u8Parity = ePARITY_NONE;
    WBR1D_UART.Frame.u8BaudID = eBAUD_9600_ID;
    WBR1D_UART.Frame.u8Stop   = eSTOP_1;
}


/*********************************************************************
 * @fn      main
 *
 * @brief   主函数
 *
 * @return  none
 */
int main()
{


    SetSysClock(CLK_SOURCE_PLL_60MHz);
    MX_GPIO_Init();

    BSP_TM16XXInterfaceInit();
    BSP_AiP8563InterfaceInit();

    WBR1D_InitialValue();
    WBR1DUartInit(&WBR1D_UART);

    MX_TIME_Init();



    

    /*  串口1  */
    GPIOA_SetBits(NEXSYS_U1_TX_PIN);
    GPIOA_ModeCfg(NEXSYS_U1_RX_PIN, GPIO_ModeIN_PU);      // RXD-配置上拉输入
    GPIOA_ModeCfg(NEXSYS_U1_TX_PIN, GPIO_ModeOut_PP_5mA); // TXD-配置推挽输出，注意先让IO口输出高电平
    UART1_DefInit();

    // 中断方式：接收数据后发送出去
    UART1_ByteTrigCfg(UART_1BYTE_TRIG);
    UART1_trigB = 7;
    UART1_INTCfg(ENABLE, RB_IER_RECV_RDY | RB_IER_LINE_STAT);
    PFIC_EnableIRQ(UART1_IRQn);

#if 0
    /*  串口3  */
    GPIOA_SetBits(RS485_MODBUS_TX_PIN);
    GPIOA_ModeCfg(RS485_MODBUS_RX_PIN, GPIO_ModeIN_PU);      // RXD-配置上拉输入
    GPIOA_ModeCfg(RS485_MODBUS_TX_PIN, GPIO_ModeOut_PP_5mA); // TXD-配置推挽输出，注意先让IO口输出高电平
    UART3_DefInit();

    // 中断方式：接收数据后发送出去
    UART3_ByteTrigCfg(UART_1BYTE_TRIG);
    UART3_trigB = 1;
    UART3_INTCfg(ENABLE, RB_IER_RECV_RDY | RB_IER_LINE_STAT);
    PFIC_EnableIRQ(UART3_IRQn);
#endif

    GPIOB_ResetBits(KEY_LED1_PIN | KEY_LED2_PIN | KEY_LED3_PIN | KEY_LED4_PIN| KEY_LED5_PIN | KEY_LED6_PIN );
    IOTModule_Reset();

    while(1)
    {
        
        SystemTimeTick_10ms();
        SystemTimeTick_100ms();
        SystemTimeTick_1000ms();

        USART_StateMachineHandler(&WBR1D_UART);

        DISPLAY_OperationTask_Test(&Display);
        LedAndKey_OperationTask_Test(&LedAndKey);
        AiP8563_OperationTask_Test(&RealTimeClock);

        
    }
}

/*********************************************************************
 * @fn      SystemTimeTick_1000s
 * @brief   1s定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_1000ms(void)
{
    if (1 == TimerFlag.u8Timer_1s)
    {
        TimerFlag.u8Timer_1s = 0;

        Display_Tick1000s(&Display);
        RealTimeClock_Tick1000s(&RealTimeClock);

        GPIOB_InverseBits(LED_LRN_PIN);
        WBR1D_Test( &WBR1D_UART.Frame );


    }
}
/*********************************************************************
 * @fn      SystemTimeTick_100ms
 * @brief   100ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_100ms(void)
{
    if (TimerFlag.u8Timer_100ms)
    {
        TimerFlag.u8Timer_100ms = 0;
    }
}
/*********************************************************************
 * @fn      SystemTimeTick_10ms
 * @brief   10ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void SystemTimeTick_10ms(void)
{
    if (TimerFlag.u8Timer_10ms)
    {
        TimerFlag.u8Timer_10ms = 0;
    }
}
/*********************************************************************
 * @fn      TimeDelay
 * @brief   延时函数
 * @param   无参数
 * @return  none
*********************************************************************/
void TimeDelay(void)
{
    TimerFlag.u8Cnt_1ms++;
    if (TimerFlag.u8Cnt_1ms >= 10)
    {
        TimerFlag.u8Cnt_1ms = 0;

        TimerFlag.u8Timer_10ms = 1;

        TimerFlag.u8Cnt_10ms++;

        if (TimerFlag.u8Cnt_10ms >= 10)
        {
            TimerFlag.u8Cnt_10ms = 0;

            TimerFlag.u8Timer_100ms = 1;

            TimerFlag.u8Cnt_100ms++;

            if (TimerFlag.u8Cnt_100ms >= 10)
            {
                TimerFlag.u8Cnt_100ms = 0;
                TimerFlag.u8Timer_1s = 1;
            }
        }
    }
}
/*********************************************************************
 * @fn      TIM0_Interrupt_1ms
 * @brief   1ms定时器
 * @param   无参数
 * @return  none
*********************************************************************/
void TIM0_Interrupt_1ms(void)
{
    USART_Time_1ms(&WBR1D_UART);


    TimeDelay();
}

/*********************************************************************
 * @fn      UART0_IRQHandler
 *
 * @brief   UART0中断函数
 *
 * @return  none
 */
__INTERRUPT
__HIGH_CODE
void UART0_IRQHandler(void)
{
    uint8_t read_dat = 0;
    uint8_t sead_dat = 0;

    switch(UART0_GetITFlag())
    {

        case UART_II_LINE_STAT: // 线路状态错误

            //UART1_SendByte(UART_II_LINE_STAT);
            //UART0_GetLinSTA();
            break;


        case UART_II_RECV_RDY: // 数据达到设置触发点

            //UART1_SendByte(UART_II_RECV_RDY);
            read_dat = UART0_RecvByte();                //UART 接收 1 个字节的数据
            URAT0_InterruptReceive(read_dat);           //串口接收函数
            UART0_CLR_RXFIFO();                         //清空FIFO缓冲区
            //UART0_SendByte(read_dat);
            break;

        case UART_II_RECV_TOUT: // 接收超时，暂时一帧数据接收完成

            //UART1_SendByte(UART_II_RECV_TOUT);

            read_dat = UART0_RecvByte();                //UART 接收 1 个字节的数据
            URAT0_InterruptReceive(read_dat);           //串口接收函数
            //UART0_CLR_RXFIFO();                         //清空FIFO缓冲区
            //UART0_SendByte(read_dat);
            break;

        case UART_II_THR_EMPTY:                       // 发送缓存区空，可继续发送

            //UART1_SendByte(UART_II_THR_EMPTY);

            if ( OK != URAT0_InterruptSend(&sead_dat) )
            {
                 LL_UART_WriteTXBuff(WBR1D_USART, sead_dat);
            }  
            break;

        case UART_II_MODEM_CHG: // 只支持串口0

            //UART1_SendByte(UART_II_MODEM_CHG);
            break;

        default:

            //UART1_SendByte(0x11);
            break;
    }

    //UART1_SendByte(0x22);
}

/*********************************************************************
 * @fn      UART1_IRQHandler
 *
 * @brief   UART1中断函数
 *
 * @return  none
 */
__INTERRUPT
__HIGH_CODE
void UART1_IRQHandler(void)
{
    volatile uint8_t i;

    switch(UART1_GetITFlag())
    {
        case UART_II_LINE_STAT: // 线路状态错误
        {
            //UART1_GetLinSTA();
            break;
        }

        case UART_II_RECV_RDY: // 数据达到设置触发点
            for(i = 0; i != UART1_trigB; i++)
            {
                UART1_RxBuff[i] = UART1_RecvByte();
                //UART1_SendByte(UART1_RxBuff[i]);
            }
            break;

        case UART_II_RECV_TOUT: // 接收超时，暂时一帧数据接收完成
            i = UART1_RecvString(UART1_RxBuff);
            //UART1_SendString(UART1_RxBuff, i);
            break;

        case UART_II_THR_EMPTY: // 发送缓存区空，可继续发送
            break;

        case UART_II_MODEM_CHG: // 只支持串口0
            break;

        default:
            break;
    }
}

/*********************************************************************
 * @fn      UART1_IRQHandler
 *
 * @brief   UART1中断函数
 *
 * @return  none
 */
__INTERRUPT
__HIGH_CODE
void UART3_IRQHandler(void)
{
    volatile uint8_t i;

    switch(UART3_GetITFlag())
    {
        case UART_II_LINE_STAT: // 线路状态错误
        {
            //UART3_GetLinSTA();
            break;
        }

        case UART_II_RECV_RDY: // 数据达到设置触发点
            for(i = 0; i != UART3_trigB; i++)
            {
                UART3_RxBuff[i] = UART3_RecvByte();
                //UART3_SendByte(UART3_RxBuff[i]);
            }
            break;

        case UART_II_RECV_TOUT: // 接收超时，暂时一帧数据接收完成
            i = UART3_RecvString(UART3_RxBuff);
            //UART3_SendString(UART3_RxBuff, i);
            break;

        case UART_II_THR_EMPTY: // 发送缓存区空，可继续发送
            break;

        case UART_II_MODEM_CHG: // 只支持串口0
            break;

        default:
            break;
    }
}
/*********************************************************************
 * @fn      TMR0_IRQHandler
 *
 * @brief   TMR0中断函数
 *
 * @return  none
 */
__INTERRUPT
__HIGH_CODE
void TMR0_IRQHandler(void) // TMR0 定时中断
{
    if(TMR0_GetITFlag(TMR0_3_IT_CYC_END))
    {
        TMR0_ClearITFlag(TMR0_3_IT_CYC_END); // 清除中断标志
        TIM0_Interrupt_1ms();
        //GPIOB_InverseBits(LED_LRN_PIN);
    }
}
