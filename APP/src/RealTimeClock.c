



#include "Common.h"
#include "BaseLib.h"

#include "RealTimeClock.h"



/*********************************************************************/
uint8_t u8AiP8563_ReadBuff[16];
uint8_t u8AiP8563_WriteBuff[16];
/*********************************************************************/
eRealTimeClock_Typedef RealTimeClock; 
/*********************************************************************/

/*********************************************************************
 * @fn      RealTimeClock_Tick1000s
 * @brief   实时时钟延时（1S）
 * @param   无参数
 * @return  none
*********************************************************************/ 
void RealTimeClock_Tick1000s(eRealTimeClock_Typedef *pRealTimeClock)
{
    if ( pRealTimeClock->u32TestWait )
    {
        pRealTimeClock->u32TestWait--;
    }
    
}
/*********************************************************************
 * @fn      BSP_AiP8563InterfaceInit
 * @brief   时钟芯片初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void BSP_AiP8563InterfaceInit(void)
{
    RealTimeClock.sAiP8563.I2C.HwPort.pGetSdaGpio = BSP_AiP8563_GetSdaGpio;

    RealTimeClock.sAiP8563.I2C.HwPort.pSetSclHigh = BSP_AiP8563_SetSclHigh;
    RealTimeClock.sAiP8563.I2C.HwPort.pSetSclLow = BSP_AiP8563_SetSclLow;
    RealTimeClock.sAiP8563.I2C.HwPort.pSetSdaHigh = BSP_AiP8563_SetSdaHigh;
    RealTimeClock.sAiP8563.I2C.HwPort.pSetSdaLow = BSP_AiP8563_SetSdaLow;

    RealTimeClock.sAiP8563.I2C.HwPort.pSetSdaInput = BSP_AiP8563_SetSdaInput;
    RealTimeClock.sAiP8563.I2C.HwPort.pSetSdaOutput = BSP_AiP8563_SetSdaOut;
    RealTimeClock.sAiP8563.I2C.HwPort.pWait = mDelayuS;

    //RealTimeClock.sAiP8563.I2C.p8Data = u8AiP8563_Buff;

    RealTimeClock.sAiP8563.pu8ReadData = u8AiP8563_ReadBuff;
    RealTimeClock.sAiP8563.pu8WriteData = u8AiP8563_WriteBuff;

    //RealTimeClock.eRunState = eRealTimeClock_IDLE;
    //AiP8563_OperationTask(&AiP8563, 300, 20);
}

/*********************************************************************
 * @fn      AiP8563_InterfaceInit
 * @brief   时钟芯片初始化
 * @param   无参数
 * @return  none
*********************************************************************/ 
void AiP8563_OperationTask_Test(eRealTimeClock_Typedef *pRealTimeClock)
{
    switch ( pRealTimeClock->eRunStateTest )
    {
    case eRealTimeClock_TEST_IDLE       :       //闲置状态

        pRealTimeClock->u32TestWait = 2; //2S
        pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_WAIT;
        break;

    case eRealTimeClock_TEST_WAIT       :        //等待

        if (pRealTimeClock->u32TestWait != 0)
        {
            break;
        }
        pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_WRITECOM;
        break;

    case eRealTimeClock_TEST_WRITECOM   :        //写入命令


        pRealTimeClock->sAiP8563.pu8WriteData[0] = 0x00;  //秒
        pRealTimeClock->sAiP8563.pu8WriteData[1] = 0x1B;  //分
        pRealTimeClock->sAiP8563.pu8WriteData[2] = 0x11;  //时
        pRealTimeClock->sAiP8563.pu8WriteData[3] = 0x0D;  //日
        pRealTimeClock->sAiP8563.pu8WriteData[4] = 0x02;  //星期
        pRealTimeClock->sAiP8563.pu8WriteData[5] = 0x09;  //月
        pRealTimeClock->sAiP8563.pu8WriteData[6] = 0x16;  //年
        //pRealTimeClock->sAiP8563.pu8WriteData[0] = 0x00;
        AiP8563_WriteSetTestMode( &pRealTimeClock->sAiP8563 );

        pRealTimeClock->u32TestWait = 1;//1S
        pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_READ_WAIT;
        break;

    case eRealTimeClock_TEST_READ_WAIT  :        //读等待

        if (pRealTimeClock->u32TestWait != 0)
        {
            break;
        }
        //pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_WRITECOM;
        pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_READ;
        break;

    case eRealTimeClock_TEST_READ       :        //读数据


        pRealTimeClock->sAiP8563.I2C.u16DataLength = 1;
        AiP8563_ReadDate( &pRealTimeClock->sAiP8563 , pRealTimeClock->u8ReadRegAdd );
        pRealTimeClock->u8ReadRegAdd++;

        if ( pRealTimeClock->u8ReadRegAdd <  0x10 )
        {
            pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_READ;
        }
        else
        {
            pRealTimeClock->u8ReadRegAdd = 0x00;
            pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_READ_WAIT;
        }
       

        pRealTimeClock->u32TestWait = 1;//1S
        break;
        
    case eRealTimeClock_END             :        //结束状态

        pRealTimeClock->eRunStateTest = eRealTimeClock_TEST_IDLE;
        break;
    
    default:
        break;
    }

}


