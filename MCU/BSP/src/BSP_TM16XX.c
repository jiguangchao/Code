

/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

#include "BSP_Include.h"



/*********************************************************************
 * @fn      BSP_TM16XX_SetSTBHigh
 * @brief   TM1629  片选引脚拉高
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetSTBHigh(void)
{
    GPIOA_SetBits(TM_STB_PIN);
}
/*********************************************************************
 * @fn      BSP_TM16XX_SetSTBLow
 * @brief   TM1629  片选引脚拉低
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetSTBLow(void)
{
    GPIOA_ResetBits(TM_STB_PIN);
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetClkHigh
 * @brief   TM1629  时钟引脚拉高
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetClkHigh(void)
{
    GPIOA_SetBits(TM_SCLK_PIN);
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetClkLow
 * @brief   TM1629  时钟引脚拉低
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetClkLow(void)
{
    GPIOA_ResetBits(TM_SCLK_PIN);
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetDataHigh
 * @brief   TM1629  数据引脚拉高
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetDataHigh(void)
{
    GPIOA_SetBits(TM_DIN_PIN);
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetDataLow
 * @brief   TM1629  数据引脚拉低
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetDataLow(void)
{
    GPIOA_ResetBits(TM_DIN_PIN);
}

/*********************************************************************
 * @fn      BSP_TM16XX_GetDataGpio
 * @brief   TM1629  读数据引脚的电平
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_TM16XX_GetDataGpio(void)
{
    //return GPIOA_ReadPortPin(TM_DIN_PIN);
    return OK;
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetDataInput
 * @brief   TM1629  设置数据引脚为输入
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetDataInput(void)
{
    GPIOA_ModeCfg(TM_DIN_PIN,GPIO_ModeIN_Floating);
}

/*********************************************************************
 * @fn      BSP_TM16XX_SetDataOut
 * @brief   TM1629  设置数据引脚为输出
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_TM16XX_SetDataOut(void)
{
    GPIOA_ModeCfg(LED_LRN_PIN,GPIO_ModeOut_PP_5mA);
}
