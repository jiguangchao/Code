


/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

#include "BSP_Include.h"




/*********************************************************************
 * @fn      BSP_AiP8563_GetSclGpio
 * @brief   读SCL引脚
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_AiP8563_GetSclGpio(void)
{
    return GPIOB_ReadPortPin( IIC_AIP_SCL_PIN );
}
/*********************************************************************
 * @fn      BSP_AiP8563_GetSdaGpio
 * @brief   读SDA引脚
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_AiP8563_GetSdaGpio(void)
{
    if ( 0x00 == GPIOB_ReadPortPin( IIC_AIP_SDA_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
    
}

/*********************************************************************
 * @fn      BSP_AiP8563_SetSclHigh
 * @brief   时钟引脚拉高
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSclHigh(void)
{
    GPIOB_SetBits( IIC_AIP_SCL_PIN );
}
/*********************************************************************
 * @fn      BSP_AiP8563_SetSclHigh
 * @brief   时钟引脚拉低
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSclLow(void)
{
    GPIOB_ResetBits( IIC_AIP_SCL_PIN );
}

/*********************************************************************
 * @fn      BSP_AiP8563_SetSdaHigh
 * @brief   数据引脚拉高
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSdaHigh(void)
{
    GPIOB_SetBits( IIC_AIP_SDA_PIN );
}
/*********************************************************************
 * @fn      BSP_AiP8563_SetSdaLow
 * @brief   数据引脚拉低
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSdaLow(void)
{
    GPIOB_ResetBits( IIC_AIP_SDA_PIN );
}

/*********************************************************************
 * @fn      BSP_AiP8563_SetSdaInput
 * @brief   设置数据引脚输入
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSdaInput(void)
{
    GPIOB_ModeCfg( IIC_AIP_SDA_PIN , GPIO_ModeIN_PU);
}
/*********************************************************************
 * @fn      BSP_AiP8563_SetSdaOut
 * @brief   设置数据引脚输出
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_AiP8563_SetSdaOut(void)
{
    GPIOB_ModeCfg( IIC_AIP_SDA_PIN , GPIO_ModeOut_PP_5mA);
}


