/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

#include "BSP_Include.h"
#include "BaseLib.h"
//#include "CH58x_common.h"



/*=============================================================================*/
/*=============================== 串口服务函数指针 ================================*/
/*=============================================================================*/
BSP_UART_ReceiveCallback_typedef URAT0_InterruptReceive;
BSP_UART_SendCallback_typedef    URAT0_InterruptSend;
BSP_UART_ReceiveCallback_typedef URAT1_InterruptReceive;
BSP_UART_SendCallback_typedef    URAT1_InterruptSend;
BSP_UART_ReceiveCallback_typedef URAT3_InterruptReceive;
BSP_UART_SendCallback_typedef    URAT3_InterruptSend;






/*=============================================================================*/
/*=============================== WBR1D  USART0 ================================*/
/*=============================================================================*/
/*********************************************************************
 * @fn      BSP_WBR1D_USART_Init
 * @brief   WBR1D串口初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART0_Init(uint8_t u8BaudID, uint8_t u8Stops, uint8_t u8Parity)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
                    
    USART_InitStruct.PrescalerValue       = LL_USART_PRESCALER_DIV2;
    
    USART_InitStruct.DataWidth            = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits             = u8Stops;
    
    USART_InitStruct.Parity               = u8Parity;

    USART_InitStruct.BaudRate             = (uint32_t)eBaudRateTab[u8BaudID];

    LL_USART_Init(WBR1D_USART, &USART_InitStruct);

    /* FIFO设置 */

    LL_USART_IsEnabledIT_FIFO(WBR1D_USART);
    //LL_USART_IsEnabledIT_FIFO_TX(WBR1D_USART);
    //LL_USART_IsEnabledIT_FIFO_RX(WBR1D_USART);
    //UART1_ByteTrigCfg(UART_1BYTE_TRIG);
    //LL_USART_IsEnabledIT_FIFO_TRIG(WBR1D_USART,0x00 << 6);//FIFO设置为1位

    /*串口中断设置*/
    //UART0_ByteTrigCfg(UART_1BYTE_TRIG);
    //LL_USART_IsEnabledIT_TX(WBR1D_USART);
    UART0_INTCfg(ENABLE, RB_IER_RECV_RDY | RB_IER_LINE_STAT);
    //UART0_INTCfg(ENABLE, RB_IER_THR_EMPTY);
    PFIC_EnableIRQ(WBR1D_USART_IRQ);
    /*
    LL_USART_IsEnabledIT_FIFO(WBR1D_USART);
    LL_USART_IsEnabledIT_RDY(WBR1D_USART);
    LL_USART_IsEnabledIT_EMPTY(WBR1D_USART);
    LL_USART_IsEnabledIT_STAT(WBR1D_USART);
    PFIC_EnableIRQ(WBR1D_USART_IRQ);
    */


}




/**********************************************************************************************/
/*                              串口0 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SendStart(uint16_t u16SendMax)
{

    uint8_t u8Data;

    LL_USART_IsEnabledIT_TX(WBR1D_USART);
    UART0_INTCfg(ENABLE, RB_IER_THR_EMPTY);

    if (OK != URAT0_InterruptSend(&u8Data))
    {
        LL_UART_WriteTXBuff(WBR1D_USART, u8Data);
    }

    return !OK;
}

/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SendStop(void)
{
    LL_UART_IsEnabledIT_RXBuffFull(WBR1D_USART);
    LL_UART_DisableIT_TXShiftBuffEmpty(WBR1D_USART);
    //FL_UART_DisableTX(WBR1D_USART);
    //FL_UART_EnableRX(WBR1D_USART);
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_DirectSend(void)
{    
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_DirectReceive(void)
{    
    return 0;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_DMA_RxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_DMA_TxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_SendOK(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_GetBusBusy(void)
{
    return OK;
}




/*=============================================================================*/
/*=============================== NEXSYS  USART1 ================================*/
/*=============================================================================*/
/*********************************************************************
 * @fn      BSP_USART1_Init
 * @brief   串口1初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART1_Init(uint8_t u8BaudID, uint8_t u8Stops, uint8_t u8Parity)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
                    
    USART_InitStruct.PrescalerValue       = LL_USART_PRESCALER_DIV2;
    
    USART_InitStruct.DataWidth            = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits             = u8Stops;
    
    USART_InitStruct.Parity               = u8Parity;

    USART_InitStruct.BaudRate             = (uint32_t)eBaudRateTab[u8BaudID];

    LL_USART_Init(NEXSYS_USART, &USART_InitStruct);

    /* FIFO设置 */

    LL_USART_IsEnabledIT_FIFO(NEXSYS_USART);


    /*串口中断设置*/

    UART1_INTCfg(ENABLE, RB_IER_RECV_RDY | RB_IER_LINE_STAT);
    PFIC_EnableIRQ(NEXSYS_USART_IRQ);



}

/**********************************************************************************************/
/*                              串口1 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SendStart(uint16_t u16SendMax)
{

    uint8_t u8Data;

    LL_USART_IsEnabledIT_TX(NEXSYS_USART);
    UART1_INTCfg(ENABLE, RB_IER_THR_EMPTY);

    if (OK != URAT1_InterruptSend(&u8Data))
    {
        LL_UART_WriteTXBuff(NEXSYS_USART, u8Data);
    }

    return !OK;
}

/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SendStop(void)
{
    LL_UART_IsEnabledIT_RXBuffFull(NEXSYS_USART);
    LL_UART_DisableIT_TXShiftBuffEmpty(NEXSYS_USART);
    //FL_UART_DisableTX(WBR1D_USART);
    //FL_UART_EnableRX(WBR1D_USART);
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_DirectSend(void)
{    
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_DirectReceive(void)
{    
    return 0;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_DMA_RxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_DMA_TxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_SendOK(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_GetBusBusy(void)
{
    return OK;
}


/*=============================================================================*/
/*=============================== MODBUS  USART3 ================================*/
/*=============================================================================*/
/*********************************************************************
 * @fn      BSP_USART3_Init
 * @brief   串口3初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART3_Init(uint8_t u8BaudID, uint8_t u8Stops, uint8_t u8Parity)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
                    
    USART_InitStruct.PrescalerValue       = LL_USART_PRESCALER_DIV2;
    
    USART_InitStruct.DataWidth            = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits             = u8Stops;
    
    USART_InitStruct.Parity               = u8Parity;

    USART_InitStruct.BaudRate             = (uint32_t)eBaudRateTab[u8BaudID];

    LL_USART_Init(RS485_USART, &USART_InitStruct);

    /* FIFO设置 */

    LL_USART_IsEnabledIT_FIFO(RS485_USART);


    /*串口中断设置*/

    UART3_INTCfg(ENABLE, RB_IER_RECV_RDY | RB_IER_LINE_STAT);
    PFIC_EnableIRQ(RS485_USART_IRQ);



}

/**********************************************************************************************/
/*                              串口3 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART3_SendStart(uint16_t u16SendMax)
{

    uint8_t u8Data;

    LL_USART_IsEnabledIT_TX(RS485_USART);
    UART3_INTCfg(ENABLE, RB_IER_THR_EMPTY);

    if (OK != URAT3_InterruptSend(&u8Data))
    {
        LL_UART_WriteTXBuff(RS485_USART, u8Data);
    }

    return !OK;
}

/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_SendStop(void)
{
    LL_UART_IsEnabledIT_RXBuffFull(RS485_USART);
    LL_UART_DisableIT_TXShiftBuffEmpty(RS485_USART);
    //FL_UART_DisableTX(WBR1D_USART);
    //FL_UART_EnableRX(WBR1D_USART);
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_DirectSend(void)
{    
    return 0;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_DirectReceive(void)
{    
    return 0;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_DMA_RxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_DMA_TxConfig(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_SendOK(void)
{
    return OK;
}
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_GetBusBusy(void)
{
    return OK;
}





