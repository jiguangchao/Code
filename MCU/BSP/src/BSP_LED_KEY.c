


/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

#include "BSP_Include.h"


/*********************************************************************/
/*                           COM灯驱动                               */
/*********************************************************************/
/*********************************************************************
 * @fn      BSP_LED_ON
 * @brief   LED亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_LED_ON(void)
{
    GPIOB_ResetBits(LED_LRN_PIN);
}
/*********************************************************************
 * @fn      BSP_LED_OFF
 * @brief   LED暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_LED_OFF(void)
{
    GPIOB_SetBits(LED_LRN_PIN);
}

/*********************************************************************/
/*                           按键灯驱动                               */
/*********************************************************************/
/*********************************************************************
 * @fn      BSP_KEY_LED1_ON
 * @brief   按键灯1亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED1_ON(void)
{
    GPIOB_SetBits(KEY_LED1_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED1_OFF
 * @brief   按键灯1暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED1_OFF(void)
{
    GPIOB_ResetBits(KEY_LED1_PIN);
}

/*********************************************************************
 * @fn      BSP_KEY_LED2_ON
 * @brief   按键灯2亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED2_ON(void)
{
    GPIOB_SetBits(KEY_LED2_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED2_OFF
 * @brief   按键灯2暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED2_OFF(void)
{
    GPIOB_ResetBits(KEY_LED2_PIN);
}

/*********************************************************************
 * @fn      BSP_KEY_LED3_ON
 * @brief   按键灯3亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED3_ON(void)
{
    GPIOB_SetBits(KEY_LED3_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED3_OFF
 * @brief   按键灯3暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED3_OFF(void)
{
    GPIOB_ResetBits(KEY_LED3_PIN);
}

/*********************************************************************
 * @fn      BSP_KEY_LED4_ON
 * @brief   按键灯4亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED4_ON(void)
{
    GPIOB_SetBits(KEY_LED4_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED4_OFF
 * @brief   按键灯4暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED4_OFF(void)
{
    GPIOB_ResetBits(KEY_LED4_PIN);
}

/*********************************************************************
 * @fn      BSP_KEY_LED5_ON
 * @brief   按键灯5亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED5_ON(void)
{
    GPIOB_SetBits(KEY_LED5_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED5_OFF
 * @brief   按键灯5暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED5_OFF(void)
{
    GPIOB_ResetBits(KEY_LED5_PIN);
}

/*********************************************************************
 * @fn      BSP_KEY_LED6_ON
 * @brief   按键灯6亮
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED6_ON(void)
{
    GPIOB_SetBits(KEY_LED6_PIN);
}
/*********************************************************************
 * @fn      BSP_KEY_LED6_OFF
 * @brief   按键灯6暗
 * @param   无参数
 * @return  none
*********************************************************************/
void BSP_KEY_LED6_OFF(void)
{
    GPIOB_ResetBits(KEY_LED6_PIN);
}



/*********************************************************************/
/*                           按键驱动                               */
/*********************************************************************/
/*********************************************************************
 * @fn      BSP_READ_KEY1
 * @brief   读按键1的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY1(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN1_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/*********************************************************************
 * @fn      BSP_READ_KEY2
 * @brief   读按键2的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY2(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN2_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/*********************************************************************
 * @fn      BSP_READ_KEY3
 * @brief   读按键3的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY3(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN3_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/*********************************************************************
 * @fn      BSP_READ_KEY4
 * @brief   读按键4的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY4(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN4_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/*********************************************************************
 * @fn      BSP_READ_KEY5
 * @brief   读按键5的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY5(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN5_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/*********************************************************************
 * @fn      BSP_READ_KEY6
 * @brief   读按键6的状态
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t BSP_READ_KEY6(void)
{
    if ( 0x00 == GPIOA_ReadPortPin( KEY_LRN6_PIN ) )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}



/*********************************************************************
 * @fn      SwitchScan
 * @brief   扫描全部按键
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t SwitchScan(void)
{
    uint8_t         u8SwitchPin;

    u8SwitchPin = 0;
    
    if (1 != BSP_READ_KEY6())
    {
        u8SwitchPin |= 0x20;
    }
    if (1 != BSP_READ_KEY5())
    {
        u8SwitchPin |= 0x10;
    }
    if (1 != BSP_READ_KEY4())
    {
        u8SwitchPin |= 0x08;
    }
    if (1 != BSP_READ_KEY3())
    {
        u8SwitchPin |= 0x04;
    }
    if (1 != BSP_READ_KEY2())
    {
        u8SwitchPin |= 0x02;
    }
    if (1 != BSP_READ_KEY1())
    {
        u8SwitchPin |= 0x01;
    }
  
    return u8SwitchPin + 16 ;
}
/**
 * @brief
 * @param
 * @retval None
 */
void SwitchReadPinStatus(uint8_t *pu8Status)
{
    uint8_t i;
    uint8_t u8Pin;
    
    uint8_t u8PinPre;

    i = 0;

    while ( i < 50 )
    {
        u8Pin = SwitchScan();

        if ( u8PinPre != u8Pin )
        {
            i = 0;
        }
        else
        {
            if ( i < 0xFF )
            {
                i++;
            }
        }
        u8PinPre = u8Pin;

        if (i < 50)
        {
            u8Pin = 0;
        }
        else
        {
            *pu8Status = u8Pin;
            break;
        }
    }
}

