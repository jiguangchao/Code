/**
  ******************************************************************************
  * @file    CH58x_ll_usart.c
  * @author  MCD Application Team
  * @brief   USART LL module driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "Common.h"
#include "CH58x_ll_usart.h"


/**
  * @brief  Initialize USART registers according to the specified
  *         parameters in USART_InitStruct.
  * @note   As some bits in USART configuration registers can only be written when the USART is disabled (USART_CR1_UE bit =0),
  *         USART Peripheral should be in disabled state prior calling this function. Otherwise, ERROR result will be returned.
  * @note   Baud rate value stored in USART_InitStruct BaudRate field, should be valid (different from 0).
  * @param  USARTx USART Instance
  * @param  USART_InitStruct pointer to a LL_USART_InitTypeDef structure
  *         that contains the configuration information for the specified USART peripheral.
  * @retval An ErrorStatus enumeration value:
  *          - SUCCESS: USART registers are initialized according to USART_InitStruct content
  *          - ERROR: Problem occurred during USART Registers initialization
  */
void LL_USART_Init( PUINT8V USARTx, LL_USART_InitTypeDef *USART_InitStruct)
{
    //配置时钟源预分频器
    LL_USART_SetPrescaler( USARTx , USART_InitStruct->PrescalerValue );
    //设置比特率
    LL_USART_SetBaudRate( USARTx , USART_InitStruct->BaudRate );
    //设置数据位
    LL_USART_SetDataWidth( USARTx , USART_InitStruct->DataWidth );
    //设置停止位
    LL_USART_SetStopBitsLength( USARTx , USART_InitStruct->StopBits );
    //设置奇偶校验位
    LL_USART_SetParity( USARTx , USART_InitStruct->Parity );


    

}
