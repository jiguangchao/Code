/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

#include "BSP_Include.h"

/*********************************************************************
 * @fn      MX_GPIO_Init
 * @brief   GPIO初始化
 * @param   无参数
 * @return  none
*********************************************************************/
void MX_GPIO_Init(void)
{
    GPIO_BEER_Init();
    GPIO_LED_Init();
    //GPIO_KEY_Init();
    //GPIO_KEY_LED_Init();
    GPIO_TM_LED_Init();

    GPIO_WBR1D_Uart0_Init();
    GPIO_NEXSYS_Uart1_Init();
    GPIO_MODBUS_Uart3_Init();

    GPIO_AIP8563_IIC_Init();
}

/*********************************************************************
 * @fn      MX_TIME_Init
 * @brief   定时器初始化
 * @param   无参数
 * @return  none
*********************************************************************/
void MX_TIME_Init(void)
{
    MX_TIM0_Init();
}

/*********************************************************************
 * @fn      GPIO_BEER_Init
 * @brief   蜂鸣器初始化
 * @param   无参数
 * @return  none
*********************************************************************/
void GPIO_BEER_Init(void)
{
    GPIOB_ModeCfg(BEER_LRN_PIN,GPIO_ModeOut_PP_5mA);
    GPIOPinRemap( ENABLE , RB_PIN_PWMX ); //复用引脚

    PWMX_CLKCfg(64);                      // 3.6KHZ
    PWMX_CycleCfg(PWMX_Cycle_256);        // 256
    PWMX_ACTOUT(CH_PWM7, 256 / 2, High_Level, ENABLE);  // 50%

    PWM7_ActDataWidth(0); //PWM 0%
}

/*********************************************************************
 * @fn      GPIO_LED_Init
 * @brief   LED初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_LED_Init(void)
{
    GPIOB_ModeCfg(LED_LRN_PIN,GPIO_ModeOut_PP_5mA);
}

/*********************************************************************
 * @fn      GPIO_KEY_Init
 * @brief   KEY初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_KEY_Init(void)
{
    GPIOA_ModeCfg(KEY_LRN1_PIN,GPIO_ModeIN_PU);
    GPIOA_ModeCfg(KEY_LRN2_PIN,GPIO_ModeIN_PU);
    GPIOA_ModeCfg(KEY_LRN3_PIN,GPIO_ModeIN_PU);
    GPIOA_ModeCfg(KEY_LRN4_PIN,GPIO_ModeIN_PU);
    GPIOA_ModeCfg(KEY_LRN5_PIN,GPIO_ModeIN_PU);
    GPIOA_ModeCfg(KEY_LRN6_PIN,GPIO_ModeIN_PU);
}

/*********************************************************************
 * @fn      GPIO_KEY_LED_Init
 * @brief   KEY_LED初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_KEY_LED_Init(void)
{
    //GPIOB_ModeCfg(KEY_LED1_PIN,GPIO_ModeOut_PP_5mA);
    //GPIOB_ModeCfg(KEY_LED2_PIN,GPIO_ModeOut_PP_5mA);
    GPIOB_ModeCfg(KEY_LED3_PIN,GPIO_ModeOut_PP_5mA);
    GPIOB_ModeCfg(KEY_LED4_PIN,GPIO_ModeOut_PP_5mA);
    GPIOB_ModeCfg(KEY_LED5_PIN,GPIO_ModeOut_PP_5mA);
    GPIOB_ModeCfg(KEY_LED6_PIN,GPIO_ModeOut_PP_5mA);
}

/*********************************************************************
 * @fn      GPIO_TM_LED_Init
 * @brief   TM_LED初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_TM_LED_Init(void)
{
    GPIOA_ModeCfg(TM_STB_PIN,GPIO_ModeOut_PP_20mA);
    GPIOA_ModeCfg(TM_SCLK_PIN,GPIO_ModeOut_PP_20mA);
    GPIOA_ModeCfg(TM_DIN_PIN,GPIO_ModeOut_PP_20mA);
}

/*********************************************************************
 * @fn      GPIO_WBR1D_Uart0_Init
 * @brief   Uart0初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_WBR1D_Uart0_Init(void)
{
    GPIOB_ModeCfg(WBR1D_U0_NRST_PIN, GPIO_ModeOut_PP_5mA); //复位引脚初始化
    GPIOB_ModeCfg(WBR1D_U0_TX_PIN, GPIO_ModeOut_PP_5mA);   //TXD-配置推挽输出，注意先让IO口输出高电平
    //GPIOB_ModeCfg(WBR1D_U0_TX_PIN, GPIO_ModeIN_PU);   //TXD-配置推挽输出，注意先让IO口输出高电平
    GPIOB_ModeCfg(WBR1D_U0_RX_PIN, GPIO_ModeIN_PU);        //RXD-配置上拉输入
    
    GPIOB_SetBits(WBR1D_U0_TX_PIN);
    GPIOB_SetBits(WBR1D_U0_NRST_PIN);

}

/*********************************************************************
 * @fn      GPIO_NEXSYS_Uart1_Init
 * @brief   Uart1初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_NEXSYS_Uart1_Init(void)
{
    GPIOB_SetBits(NEXSYS_U1_TX_PIN);
    GPIOA_ModeCfg(NEXSYS_U1_TX_PIN, GPIO_ModeOut_PP_5mA);   //TXD-配置推挽输出，注意先让IO口输出高电平
    GPIOA_ModeCfg(NEXSYS_U1_RX_PIN, GPIO_ModeIN_PU);        //RXD-配置上拉输入
    
    UART1_BaudRateCfg(115200); //波特率115200
    UART1_DefInit();
}

/*********************************************************************
 * @fn      GPIO_MODBUS_Uart3_Init
 * @brief   Uart3初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_MODBUS_Uart3_Init(void)
{
    GPIOB_ModeCfg(RS485_MODBUS_DE_PIN, GPIO_ModeOut_PP_5mA); //复位引脚初始化
    GPIOB_SetBits(RS485_MODBUS_TX_PIN);
    GPIOB_ModeCfg(RS485_MODBUS_TX_PIN, GPIO_ModeOut_PP_5mA);   //TXD-配置推挽输出，注意先让IO口输出高电平
    GPIOB_ModeCfg(RS485_MODBUS_RX_PIN, GPIO_ModeIN_PU);        //RXD-配置上拉输入
    
    UART3_BaudRateCfg(9600); //波特率9600
    UART3_DefInit();
}

/*********************************************************************
 * @fn      GPIO_AIP8563_IIC_Init
 * @brief   IIC初始化
 * @param   none
 * @return  none
*********************************************************************/
void GPIO_AIP8563_IIC_Init(void)
{
    GPIOB_ModeCfg(IIC_AIP_SCL_PIN, GPIO_ModeOut_PP_5mA);
    GPIOB_ModeCfg(IIC_AIP_SDA_PIN, GPIO_ModeOut_PP_5mA);

    GPIOB_SetBits( IIC_AIP_SCL_PIN |  IIC_AIP_SDA_PIN );

    //I2C_Init(I2C_Mode_I2C, 400000, I2C_DutyCycle_16_9, I2C_Ack_Enable, I2C_AckAddr_7bit, IIC_W_ADD);
}
/*********************************************************************
 * @fn      MX_TIM0_Init
 * @brief   定时器0初始化
 * @param   none
 * @return  none
*********************************************************************/
void MX_TIM0_Init(void)
{
    TMR0_TimerInit(FREQ_SYS / 1000);         // 设置定时时间 1ms
    TMR0_ITCfg(ENABLE, TMR0_3_IT_CYC_END); // 开启中断
    PFIC_EnableIRQ(TMR0_IRQn);
}


