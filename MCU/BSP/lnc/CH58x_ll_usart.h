/**
  ******************************************************************************
  * @file    CH58x_ll_usart.h
  * @author  MCD Application Team
  * @brief   Header file of USART LL module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CH58x_LL_USART_H
#define CH58x_LL_USART_H


#ifdef __cplusplus
extern "C" {
#endif



#include "CH58x_common.h"
#include "Common.h"


/**
  * @brief    SET_BIT
  * @param    REG 寄存器地址
  * @param    BIT 数据
  * @retval   Nome
  */
static inline void SET_BIT(PUINT8V REG,uint8_t BIT)
{
    *REG |= BIT;
}

/**
  * @brief    CLEAR_BIT
  * @param    REG 寄存器地址
  * @param    BIT 数据
  * @retval   Nome
  */
static inline void CLEAR_BIT(PUINT8V REG,uint8_t BIT)
{
    *REG &= ~BIT;
}

/**
  * @brief    READ_BIT
  * @param    REG 寄存器地址
  * @param    BIT 数据
  * @retval   Nome
  */
static inline uint8_t READ_BIT(PUINT8V REG,uint8_t BIT)
{
    return  *REG & BIT;
}

/**
  * @brief    CLEAR_REG
  * @param    REG 寄存器地址
  * @retval   Nome
  */
static inline void CLEAR_REG(PUINT8V REG)
{
    *REG = 0x00;
}
/*********************************************************************
 * @fn      WRITE_REG
 * @brief   写入8位寄存器
 * @param    REG 寄存器地址
 * @param    VAL 数据
 * @return  none
*********************************************************************/
static inline void WRITE_REG(PUINT8V REG,uint8_t VAL)
{
    *REG = VAL;
}

/**
  * @brief    READ_REG
  * @param    REG 寄存器地址
  * @retval   Nome
  */
static inline uint8_t READ_REG(PUINT8V REG)
{
    return *REG ;
}

/**
  * @brief    MODIFY_REG
  * @param    REG         寄存器地址
  * @param    CLEARMASK   写入寄存器的位
  * @param    SETMASK     写入的内容
  * @retval   Nome
  */
static inline void MODIFY_REG(PUINT8V REG ,uint8_t CLEARMASK,uint8_t SETMASK)
{
    WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)));
}




/* Array used to get the USART prescaler division decimal values versus @ref USART_LL_EC_PRESCALER values */
static const UINT32 USART_PRESCALER_TAB[] =
{
  1UL,
  2UL,
  4UL,
  6UL,
  8UL,
  10UL,
  12UL,
  16UL,
  32UL,
  64UL,
  128UL,
  256UL
};

/**
  * @brief LL USART Init Structure definition
  */
typedef struct
{
  uint8_t PrescalerValue;            /*!< 指定用于计算通信波特率的预分频器。
                                           This parameter can be a value of @ref USART_LL_EC_PRESCALER.

                                           This feature can be modified afterwards using unitary function @ref LL_USART_SetPrescaler().*/

  uint32_t BaudRate;                  /*!< 该字段定义了预期的 Usart 通信波特率。

                                           This feature can be modified afterwards using unitary function @ref LL_USART_SetBaudRate().*/

  uint8_t DataWidth;                 /*!< 指定帧中传输或接收的数据位数。
                                           This parameter can be a value of @ref USART_LL_EC_DATAWIDTH.

                                           This feature can be modified afterwards using unitary function @ref LL_USART_SetDataWidth().*/

  uint8_t StopBits;                  /*!< 指定传输的停止位数。
                                           This parameter can be a value of @ref USART_LL_EC_STOPBITS.

                                           This feature can be modified afterwards using unitary function @ref LL_USART_SetStopBitsLength().*/

  uint8_t Parity;                    /*!< 指定奇偶校验模式。
                                           This parameter can be a value of @ref USART_LL_EC_PARITY.

                                           This feature can be modified afterwards using unitary function @ref LL_USART_SetParity().*/
                            

} LL_USART_InitTypeDef;
/*******************  Bit definition for USART_PRESC register  ****************/
#define USART_PRESC_PRESCALER_Pos    (0U)
#define USART_PRESC_PRESCALER_Msk    (0xFUL << USART_PRESC_PRESCALER_Pos)      /*!< 0x0000000F */
#define USART_PRESC_PRESCALER        USART_PRESC_PRESCALER_Msk                 /*!< PRESCALER[3:0] bits (Clock prescaler) */
#define USART_PRESC_PRESCALER_0      (0x1UL << USART_PRESC_PRESCALER_Pos)      /*!< 0x00000001 */
#define USART_PRESC_PRESCALER_1      (0x2UL << USART_PRESC_PRESCALER_Pos)      /*!< 0x00000002 */
#define USART_PRESC_PRESCALER_2      (0x4UL << USART_PRESC_PRESCALER_Pos)      /*!< 0x00000004 */
#define USART_PRESC_PRESCALER_3      (0x8UL << USART_PRESC_PRESCALER_Pos)      /*!< 0x00000008 */

/** @defgroup USART_LL_EC_PRESCALER Clock Source Prescaler
  * @{
  */
#define LL_USART_PRESCALER_DIV1                 0x00U                                                                   /*!< Input clock not devided   */
#define LL_USART_PRESCALER_DIV2                 (USART_PRESC_PRESCALER_0)                                                     /*!< Input clock devided by 2  */
#define LL_USART_PRESCALER_DIV4                 (USART_PRESC_PRESCALER_1)                                                     /*!< Input clock devided by 4  */
#define LL_USART_PRESCALER_DIV6                 (USART_PRESC_PRESCALER_1 | USART_PRESC_PRESCALER_0)                           /*!< Input clock devided by 6  */
#define LL_USART_PRESCALER_DIV8                 (USART_PRESC_PRESCALER_2)                                                     /*!< Input clock devided by 8  */
#define LL_USART_PRESCALER_DIV10                (USART_PRESC_PRESCALER_2 | USART_PRESC_PRESCALER_0)                           /*!< Input clock devided by 10 */
#define LL_USART_PRESCALER_DIV12                (USART_PRESC_PRESCALER_2 | USART_PRESC_PRESCALER_1)                           /*!< Input clock devided by 12 */
#define LL_USART_PRESCALER_DIV16                (USART_PRESC_PRESCALER_2 | USART_PRESC_PRESCALER_1 | USART_PRESC_PRESCALER_0) /*!< Input clock devided by 16 */
#define LL_USART_PRESCALER_DIV32                (USART_PRESC_PRESCALER_3)                                                     /*!< Input clock devided by 32 */
#define LL_USART_PRESCALER_DIV64                (USART_PRESC_PRESCALER_3 | USART_PRESC_PRESCALER_0)                           /*!< Input clock devided by 64 */
#define LL_USART_PRESCALER_DIV128               (USART_PRESC_PRESCALER_3 | USART_PRESC_PRESCALER_1)                           /*!< Input clock devided by 128 */
#define LL_USART_PRESCALER_DIV256               (USART_PRESC_PRESCALER_3 | USART_PRESC_PRESCALER_1 | USART_PRESC_PRESCALER_0) /*!< Input clock devided by 256 */

/** @defgroup USART_LL_EC_DATAWIDTH Datawidth
  * @{
  */
#define USART_DATAWIDTH_Pos                     (0U)
#define LL_USART_DATAWIDTH_5B                   (0x00U << USART_DATAWIDTH_Pos )        /*!< 5 bits word length : Start bit, 7 data bits, n stop bits */
#define LL_USART_DATAWIDTH_6B                   (0x01U << USART_DATAWIDTH_Pos )        /*!< 6 bits word length : Start bit, 7 data bits, n stop bits */
#define LL_USART_DATAWIDTH_7B                   (0x02U << USART_DATAWIDTH_Pos )        /*!< 7 bits word length : Start bit, 8 data bits, n stop bits */
#define LL_USART_DATAWIDTH_8B                   (0x03U << USART_DATAWIDTH_Pos )        /*!< 8 bits word length : Start bit, 9 data bits, n stop bits */
/**
  * @}
  */

/** @defgroup USART_LL_EC_STOPBITS Stop Bits
  * @{
  */
#define USART_STOP_Pos                          (2U)
#define LL_USART_STOPBITS_1                     (0x00U << USART_STOP_Pos )              /*!< 1 stop bit */
#define LL_USART_STOPBITS_2                     (0x01U << USART_STOP_Pos )              /*!< 2 stop bits */
/**
  * @}
  */


/** @defgroup USART_LL_EC_PARITY Parity Control
  * @{
  */
#define USART_PARITY_Pos                        (3U)
#define LL_USART_PARITY_NOEN                    (0x00U << USART_PARITY_Pos )               /*!< 无奇偶校验 */
#define LL_USART_PARITY_ODD                     (0x01U << USART_PARITY_Pos )               /*!< 奇校验 */
#define LL_USART_PARITY_EVEN                    (0x02U << USART_PARITY_Pos )               /*!< 偶校验 */
/**
  * @}
  */

/**
  * @brief  为波特率发生器和过采样配置时钟源预分频器
  * @note   Macro @ref IS_UART_FIFO_INSTANCE(USARTx) can be used to check whether or not
  *         FIFO mode feature is supported by the USARTx instance.
  * @rmtoll PRESC        PRESCALER     LL_USART_SetPrescaler
  * @param  USARTx USART Instance
  * @param  PrescalerValue This parameter can be one of the following values:
  *         @arg @ref LL_USART_PRESCALER_DIV1
  *         @arg @ref LL_USART_PRESCALER_DIV2
  *         @arg @ref LL_USART_PRESCALER_DIV4
  *         @arg @ref LL_USART_PRESCALER_DIV6
  *         @arg @ref LL_USART_PRESCALER_DIV8
  *         @arg @ref LL_USART_PRESCALER_DIV10
  *         @arg @ref LL_USART_PRESCALER_DIV12
  *         @arg @ref LL_USART_PRESCALER_DIV16
  *         @arg @ref LL_USART_PRESCALER_DIV32
  *         @arg @ref LL_USART_PRESCALER_DIV64
  *         @arg @ref LL_USART_PRESCALER_DIV128
  *         @arg @ref LL_USART_PRESCALER_DIV256
  * @retval None
  */
static inline void LL_USART_SetPrescaler(PUINT8V UARTx, uint8_t PrescalerValue)
{
    WRITE_REG( (UARTx + UART_DIV) , PrescalerValue);
}

/**
  * @brief    设置波特率
  * @rmtoll   ISR    PERR    LL_USART_SetBaudRate
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_SetBaudRate(PUINT8V UARTx, uint32_t baudrate)
{
    uint32_t u32Data;

    u32Data = 10 * GetSysClock() / 8 / baudrate;
    u32Data = (u32Data + 5) / 10;
    //R16_UART0_DL = (uint16_t)x;

    if ( BA_UART0 == UARTx )
    {
    	R16_UART0_DL = (uint16_t)u32Data;
    }
    else if ( BA_UART1 == UARTx  )
	{
		R16_UART1_DL = (uint16_t)u32Data;
	}
	else if ( BA_UART2 == UARTx  )
	{
		R16_UART2_DL = (uint16_t)u32Data;
	}
	else if ( BA_UART3 == UARTx  )
	{
		R16_UART3_DL = (uint16_t)u32Data;
	}

    
    //WRITE_REG( (UARTx + UART_DLL) , ( (uint8_t)(u32Data)) );
    //WRITE_REG( (UARTx + UART_DLM) , ( ((uint8_t)(u32Data))>>8) );
    
}

/**
  * @brief  设置数据位
  * @rmtoll CR1          M0            LL_USART_SetDataWidth\n
  *         CR1          M1            LL_USART_SetDataWidth
  * @param  USARTx USART Instance
  * @param  DataWidth This parameter can be one of the following values:
  *         @arg @ref LL_USART_DATAWIDTH_5B
  *         @arg @ref LL_USART_DATAWIDTH_6B
  *         @arg @ref LL_USART_DATAWIDTH_7B
  *   *     @arg @ref LL_USART_DATAWIDTH_8B
  * @retval None
  */
static inline void LL_USART_SetDataWidth(PUINT8V USARTx, uint8_t DataWidth)
{
  MODIFY_REG( (USARTx + UART_LCR ), RB_LCR_WORD_SZ , DataWidth);
}

/**
  * @brief  设置停止位
  * @rmtoll CR2          STOP          LL_USART_SetStopBitsLength
  * @param  USARTx USART Instance
  * @param  StopBits This parameter can be one of the following values:
  *         @arg @ref LL_USART_STOPBITS_1
  *         @arg @ref LL_USART_STOPBITS_2
  * @retval None
  */
static inline void LL_USART_SetStopBitsLength(PUINT8V USARTx, uint8_t StopBits)
{
  MODIFY_REG( (USARTx + UART_LCR ), RB_LCR_STOP_BIT, StopBits);
}

/**
  * @brief  设置奇偶校验位
  * @note   This function selects if hardware parity control (generation and detection) is enabled or disabled.
  *         When the parity control is enabled (Odd or Even), computed parity bit is inserted at the MSB position
  *         (9th or 8th bit depending on data width) and parity is checked on the received data.
  * @rmtoll CR1          PS            LL_USART_SetParity\n
  *         CR1          PCE           LL_USART_SetParity
  * @param  USARTx USART Instance
  * @param  Parity This parameter can be one of the following values:
  *         @arg @ref LL_USART_PARITY_NONE
  *         @arg @ref LL_USART_PARITY_EVEN
  *         @arg @ref LL_USART_PARITY_ODD
  * @retval None
  */
static inline void LL_USART_SetParity(PUINT8V USARTx, uint8_t Parity)
{
   MODIFY_REG( (USARTx + UART_LCR ), (RB_LCR_PAR_EN | RB_LCR_PAR_MOD), Parity);
}

/**
  * @brief    接收数据中断使能位
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_RDY
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_RDY(PUINT8V UARTx)
{
    SET_BIT((UARTx +UART_IER), RB_IER_RECV_RDY );
}

/**
  * @brief    接收数据中断使能位
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_RDY
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_EMPTY(PUINT8V UARTx)
{
    SET_BIT((UARTx +UART_IER), RB_IER_THR_EMPTY );
}

/**
  * @brief    接收线路状态中断使能位
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_RDY
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_STAT(PUINT8V UARTx)
{
    SET_BIT((UARTx +UART_IER), RB_IER_LINE_STAT );
}

/**
  * @brief    接收线路状态中断使能位
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_RDY
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_TX(PUINT8V UARTx)
{
    SET_BIT((UARTx +UART_IER), RB_IER_TXD_EN );
}

/**
  * @brief    触发串口中断
  * @rmtoll   ISR    PERR    LL_UART_EnableTX
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_EnableTX(PUINT8V UARTx , uint8_t Trig)
{
    SET_BIT((UARTx +UART_IIR), RB_MCR_OUT2 );
    //MODIFY_REG( (UARTx + UART_IIR), RB_IIR_INT_MASK, Trig);
}

/**
  * @brief    设置FIFO禁用
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_FIFO
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_FIFO(PUINT8V UARTx)
{
    CLEAR_BIT((UARTx + UART_FCR), RB_FCR_FIFO_EN) ;
}

/**
  * @brief    设置FIFO使能
  * @rmtoll   ISR    PERR    LL_USART_EnabledIT_FIFO
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_EnabledIT_FIFO(PUINT8V UARTx)
{
    SET_BIT((UARTx + UART_FCR), RB_FCR_FIFO_EN) ;
}
/**
  * @brief    设置FIFO的TX使能
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_FIFO
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_FIFO_TX(PUINT8V UARTx)
{
    SET_BIT((UARTx + UART_FCR), RB_FCR_TX_FIFO_CLR) ;
}
/**
  * @brief    设置FIFO的RX使能
  * @rmtoll   ISR    PERR    LL_USART_IsEnabledIT_FIFO
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_USART_IsEnabledIT_FIFO_RX(PUINT8V UARTx)
{
    SET_BIT((UARTx + UART_FCR), RB_FCR_RX_FIFO_CLR) ;
}
/**
  * @brief  设置FIFO触发控制点
  * @note   This function selects if hardware parity control (generation and detection) is enabled or disabled.
  *         When the parity control is enabled (Odd or Even), computed parity bit is inserted at the MSB position
  *         (9th or 8th bit depending on data width) and parity is checked on the received data.
  * @rmtoll CR1          PS            LL_USART_SetParity\n
  *         CR1          PCE           LL_USART_SetParity
  * @param  USARTx USART Instance
  * @param  Trig This parameter can be one of the following values:
  * @retval None
  */
static inline void LL_USART_IsEnabledIT_FIFO_TRIG(PUINT8V USARTx, uint8_t Trig)
{
   MODIFY_REG( (USARTx + UART_FCR ), RB_FCR_FIFO_TRIG , Trig);
}


/**
  * @brief    接收FIFO错误标志位
  * @rmtoll   ISR    PERR    LL_UART_IsActiveFlag_FIFOError
  * @param    UARTx UART instance
  * @retval   State of bit (1 or 0).
  */
static inline uint8_t LL_UART_IsActiveFlag_FIFOError(PUINT8V UARTx)
{
    return (uint8_t)((READ_BIT(( UARTx + UART_LSR ) , RB_LSR_ERR_RX_FIFO) == (RB_LSR_ERR_RX_FIFO)) ? 1UL : 0UL );
}

/**
  * @brief    清除接收FIFO错误标志位
  * @rmtoll   ISR    PERR    LL_UART_ClearFlag_FIFOError
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_ClearFlag_FIFOError(PUINT8V UARTx)
{
    WRITE_REG(( UARTx + UART_LSR ), RB_LSR_ERR_RX_FIFO);
}

/**
  * @brief    接收数据奇偶校验错误标志位
  * @rmtoll   ISR    PERR    LL_UART_IsActiveFlag_ParityError
  * @param    UARTx UART instance
  * @retval   State of bit (1 or 0).
  */
static inline uint8_t LL_UART_IsActiveFlag_ParityError(PUINT8V UARTx)
{
    return (uint8_t)((READ_BIT(( UARTx + UART_LSR ) , RB_LSR_PAR_ERR) == (RB_LSR_PAR_ERR)) ? 1UL : 0UL );
}

/**
  * @brief    清除 UART 奇偶校验错误标志
  * @rmtoll   ISR    PERR    LL_UART_ClearFlag_ParityError
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_ClearFlag_ParityError(PUINT8V UARTx)
{
    WRITE_REG(( UARTx + UART_LSR ), RB_LSR_PAR_ERR);
}

/**
  * @brief    接收FIFO缓冲区满标志
  * @rmtoll   ISR    PERR    LL_UART_IsEnabledIT_RXBuffFull
  * @param    UARTx UART instance
  * @retval   State of bit (1 or 0).
  */
static inline uint8_t LL_UART_IsEnabledIT_RXBuffFull(PUINT8V UARTx)
{
    return (uint8_t)(READ_BIT(( UARTx + UART_LSR ) , RB_LSR_OVER_ERR) == (RB_LSR_OVER_ERR));
}

/**
  * @brief    /清除 UART 接收FIFO缓冲区满标志
  * @rmtoll   ISR    RXBF    LL_UART_ClearFlag_RXBuffFull
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_ClearFlag_RXBuffFull(PUINT8V UARTx)
{
    WRITE_REG(( UARTx + UART_LSR ), RB_LSR_OVER_ERR);
}


/**
  * @brief    发送保持寄存器 THR 和发送移位寄存器TSR 全空标志位
  * @rmtoll   ISR    PERR    LL_UART_IsActiveFlag_TXShiftBuffEmpty
  * @param    UARTx UART instance
  * @retval   State of bit (1 or 0).
  */
static inline uint8_t LL_UART_IsActiveFlag_TXShiftBuffEmpty(PUINT8V UARTx)
{
    return (uint8_t)((READ_BIT(( UARTx + UART_LSR ) , RB_LSR_TX_ALL_EMP ) == (RB_LSR_TX_ALL_EMP)) ? 1UL : 0UL);
}

/**
  * @brief    清空发送移位寄存器空标志
  * @rmtoll   ISR    TXSE    LL_UART_ClearFlag_TXShiftBuffEmpty
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_ClearFlag_TXShiftBuffEmpty(PUINT8V UARTx)
{
    WRITE_REG(( UARTx + UART_LSR ) , RB_LSR_TX_ALL_EMP );
}


/**
  * @brief    禁用发送寄存器空中断
  * @rmtoll   IER    TXSEIE    FL_UART_DisableIT_TXShiftBuffEmpty
  * @param    UARTx UART instance
  * @retval   None
  */
static inline void LL_UART_DisableIT_TXShiftBuffEmpty(PUINT8V UARTx)
{
    CLEAR_BIT( (UARTx + UART_IER ), RB_IER_THR_EMPTY );
}


/**
  * @brief    写入单个字节
  * @rmtoll   TXBUF        FL_UART_WriteTXBuff
  * @param    UARTx UART instance
  * @param    data
  * @retval   None
  */
static inline void LL_UART_WriteTXBuff(PUINT8V UARTx, uint8_t data)
{
    WRITE_REG( (UARTx + UART_THR), data);
}

/**
  * @brief    读单个字节
  * @rmtoll   RXBUF        FL_UART_ReadRXBuff
  * @param    UARTx UART instance
  * @retval
  */
static inline uint8_t LL_UART_ReadRXBuff(PUINT8V UARTx)
{
    return READ_REG(UARTx + UART_RBR);
}



void LL_USART_Init( PUINT8V USARTx, LL_USART_InitTypeDef *USART_InitStruct);


#ifdef __cplusplus
}
#endif

#endif /* CH58x_LL_USART_H */
