#ifndef _BSP_USART_H_
#define _BSP_USART_H_


typedef uint8_t (*BSP_UART_ReceiveCallback_typedef)(uint8_t u8RecData);
typedef uint8_t (*BSP_UART_SendCallback_typedef)(uint8_t *pu8SendData);

extern BSP_UART_ReceiveCallback_typedef URAT0_InterruptReceive;
extern BSP_UART_SendCallback_typedef    URAT0_InterruptSend;
extern BSP_UART_ReceiveCallback_typedef URAT1_InterruptReceive;
extern BSP_UART_SendCallback_typedef    URAT1_InterruptSend;
extern BSP_UART_ReceiveCallback_typedef URAT3_InterruptReceive;
extern BSP_UART_SendCallback_typedef    URAT3_InterruptSend;

/*********************************************************************
 * @fn      BSP_USART0_Init
 * @brief   串口0初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART0_Init(uint8_t u8Baud, uint8_t u8Stops, uint8_t u8Parity);


/**********************************************************************************************/
/*                              串口0 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SendStart(uint16_t u16SendMax);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SendStop(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_DirectSend(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_DirectReceive(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_DMA_RxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_DMA_TxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_SendOK(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART0_GetBusBusy(void);

/*=============================================================================*/
/*=============================== NEXSYS  USART1 ================================*/
/*=============================================================================*/
/*********************************************************************
 * @fn      BSP_USART1_Init
 * @brief   串口1初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART1_Init(uint8_t u8BaudID, uint8_t u8Stops, uint8_t u8Parity);

/**********************************************************************************************/
/*                              串口1 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SendStart(uint16_t u16SendMax);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SendStop(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_DirectSend(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_DirectReceive(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_DMA_RxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_DMA_TxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_SendOK(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART1_GetBusBusy(void);

/*=============================================================================*/
/*=============================== MODBUS  USART3 ================================*/
/*=============================================================================*/
/*********************************************************************
 * @fn      BSP_USART3_Init
 * @brief   串口3初始化
 * @param   u8Baud     波特率
 * @param   u8Stops    停止位
 * @param   u8Parity   奇偶校验位
 * @return  none
*********************************************************************/
void BSP_USART3_Init(uint8_t u8BaudID, uint8_t u8Stops, uint8_t u8Parity);

/**********************************************************************************************/
/*                              串口3 设置初始化函数                                           */
/**********************************************************************************************/
/**
  * @brief  
  * @param
  * @retval None
  */
uint8_t BSP_UART3_SendStart(uint16_t u16SendMax);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_SendStop(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_DirectSend(void);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART3_DirectReceive(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_DMA_RxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_DMA_TxConfig(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_SendOK(void);
/**
 * @brief
 * @param
 * @retval None
 */
uint8_t BSP_UART3_GetBusBusy(void);




#endif

/* End of file */