/********************************** (C) COPYRIGHT *******************************
 * File Name          : BSP_IOMap.h
 * Author             : WCH
 * Version            : V1.0
 * Date               : 2022.8.24
 * Description
 * Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
 * SPDX-License-Identifier: Apache-2.0
 *******************************************************************************/
#ifndef _BSP_IO_MAP_H_
#define _BSP_IO_MAP_H_

#include "Common.h"
/******************************************************************************/
/*  USART  Data */
/******************************************************************************/
#define WBR1D_USART                         (BA_UART0)
#define WBR1D_USART_IRQ                     (UART0_IRQn)
#define WBR1D_TX_BUFF                       (64)
#define WBR1D_RX_BUFF                       (64)


#define NEXSYS_USART                        (BA_UART1)
#define NEXSYS_USART_IRQ                    (UART1_IRQn)
#define NEXSYS_TX_BUFF                      (64)
#define NEXSYS_RX_BUFF                      (64)

#define RS485_USART                        (BA_UART3)
#define RS485_USART_IRQ                    (UART3_IRQn)
#define RS485_MODBUS_TX_BUFF                (128)
#define RS485_MODBUS_RX_BUFF                (128)


/******************************************************************************/
/* 蜂鸣器 BEER PIN IN MCU */
/******************************************************************************/
#define BEER_LRN_PIN                                (GPIO_Pin_1)  //PB1

/******************************************************************************/
/*  LED PIN IN MCU */
/******************************************************************************/
#define LED_LRN_PIN                                 (GPIO_Pin_0)  //PB0


/******************************************************************************/
/* KEY PIN IN MCU */
/******************************************************************************/
#define KEY_LRN1_PIN                                 (GPIO_Pin_5)  //PA5
#define KEY_LRN2_PIN                                 (GPIO_Pin_6)  //PA6
#define KEY_LRN3_PIN                                 (GPIO_Pin_0)  //PA0
#define KEY_LRN4_PIN                                 (GPIO_Pin_1)  //PA1
#define KEY_LRN5_PIN                                 (GPIO_Pin_2)  //PA2
#define KEY_LRN6_PIN                                 (GPIO_Pin_3)  //PA3
/******************************************************************************/
/* KEY LED PIN IN MCU */
/******************************************************************************/

#define KEY_LED1_PIN                                 (GPIO_Pin_14)  //PB14
#define KEY_LED2_PIN                                 (GPIO_Pin_15)  //PB15
#define KEY_LED3_PIN                                 (GPIO_Pin_16)  //PB16
#define KEY_LED4_PIN                                 (GPIO_Pin_17)  //PB17
#define KEY_LED5_PIN                                 (GPIO_Pin_8)   //PB8
#define KEY_LED6_PIN                                 (GPIO_Pin_9)   //PB9

/******************************************************************************/
/* TM1629 LED PIN IN MCU */
/******************************************************************************/
#define TM_STB_PIN                                   (GPIO_Pin_13)  //PA13
#define TM_SCLK_PIN                                  (GPIO_Pin_14)  //PA14
#define TM_DIN_PIN                                   (GPIO_Pin_15)  //PA15

/******************************************************************************/
/* WBR1D -- UART0  RX->PB4 TX->PB7 RST->PB6 */
/******************************************************************************/
#define WBR1D_U0_RX_PIN                              (GPIO_Pin_4)  //PB4
#define WBR1D_U0_TX_PIN                              (GPIO_Pin_7)  //PB7
#define WBR1D_U0_NRST_PIN                            (GPIO_Pin_6)  //PB6

/******************************************************************************/
/* NEXSYS -- UART1  RX->PA8 TX->PA9  */
/******************************************************************************/
#define NEXSYS_U1_RX_PIN                              (GPIO_Pin_8)  //PA8
#define NEXSYS_U1_TX_PIN                              (GPIO_Pin_9)  //PA9

/******************************************************************************/
/* RS485 Cascade  -- uart3  RX->PB20 TX->PB21 EN3->PB19 */
/******************************************************************************/    
#define RS485_MODBUS_TX_PIN                           (GPIO_Pin_21) //PB21
#define RS485_MODBUS_RX_PIN                           (GPIO_Pin_20) //PB20
#define RS485_MODBUS_DE_PIN                           (GPIO_Pin_19) //PB19

/******************************************************************************/
/*  AiP8563 SDA->PB12 SCL->PB13 */
/******************************************************************************/

#define IIC_AIP_SDA_PIN                              (GPIO_Pin_12) //PB12
#define IIC_AIP_SCL_PIN                              (GPIO_Pin_13) //PB13




#endif
