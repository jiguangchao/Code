#ifndef _COMMON_H_
#define _COMMON_H_

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

/** @addtogroup Exported_macros
  * @{
  */




#include <string.h>
#include <stdlib.h>
#include <errno.h>


#include "CH58x_common.h"
#include "BSP_Include.h"




#if 0

#ifdef  SET_BIT
#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#endif

#ifdef  CLEAR_BIT
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
#endif

#ifdef  READ_BIT
#define READ_BIT(REG, BIT)    ((REG) & (BIT))
#endif

#ifdef  CLEAR_REG
#define CLEAR_REG(REG)        ((REG) = (0x0))
#endif

#ifdef  WRITE_REG
#define WRITE_REG(REG, VAL)   ((REG) = (VAL))
#endif

#ifdef  READ_REG
#define READ_REG(REG)         ((REG))
#endif

#ifdef  MODIFY_REG
#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))
#endif

/* Use of interrupt control for register exclusive access */
/* Atomic 32-bit register access macro to set one or several bits */
#define ATOMIC_SET_BIT(REG, BIT)                             \
  do {                                                       \
    uint32_t primask;                                        \
    primask = __get_PRIMASK();                               \
    __set_PRIMASK(1);                                        \
    SET_BIT((REG), (BIT));                                   \
    __set_PRIMASK(primask);                                  \
  } while(0)

/* Atomic 32-bit register access macro to clear one or several bits */
#define ATOMIC_CLEAR_BIT(REG, BIT)                           \
  do {                                                       \
    uint32_t primask;                                        \
    primask = __get_PRIMASK();                               \
    __set_PRIMASK(1);                                        \
    CLEAR_BIT((REG), (BIT));                                 \
    __set_PRIMASK(primask);                                  \
  } while(0)

/* Atomic 32-bit register access macro to clear and set one or several bits */
#define ATOMIC_MODIFY_REG(REG, CLEARMSK, SETMASK)            \
  do {                                                       \
    uint32_t primask;                                        \
    primask = __get_PRIMASK();                               \
    __set_PRIMASK(1);                                        \
    MODIFY_REG((REG), (CLEARMSK), (SETMASK));                \
    __set_PRIMASK(primask);                                  \
  } while(0)

/* Atomic 16-bit register access macro to set one or several bits */
#define ATOMIC_SETH_BIT(REG, BIT) ATOMIC_SET_BIT(REG, BIT)                                   \

/* Atomic 16-bit register access macro to clear one or several bits */
#define ATOMIC_CLEARH_BIT(REG, BIT) ATOMIC_CLEAR_BIT(REG, BIT)                               \

/* Atomic 16-bit register access macro to clear and set one or several bits */
#define ATOMIC_MODIFYH_REG(REG, CLEARMSK, SETMASK) ATOMIC_MODIFY_REG(REG, CLEARMSK, SETMASK) \

#endif

/*********************************************************************************/
typedef enum
{
    eBAUD_1200_ID = 0,
    eBAUD_2400_ID, 
    eBAUD_4800_ID, 
    eBAUD_9600_ID, 
    eBAUD_19200_ID,
    eBAUD_38400_ID,
    eBAUD_57600_ID,
    eBAUD_115200_ID,
}eUSART_BAUD_ID_TYPE;

typedef enum
{
    eBAUD_1200 = 1200,
    eBAUD_2400 = 2400,
    eBAUD_4800 = 4800,
    eBAUD_9600 = 9600,
    eBAUD_19200 = 19200,
    eBAUD_38400 = 38400,
    eBAUD_57600 = 57600,
    eBAUD_115200 = 115200,
}eUSART_BAUD_TYPE;

typedef enum
{
    eSTOP_1 = 0,
    eSTOP_2 = 1,
}eUSART_STOP_TYPE;

typedef enum
{
    ePARITY_NONE = 0,
    ePARITY_EVEN ,
    ePARITY_ODD ,
}eUSART_PARITY_TYPE;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif


