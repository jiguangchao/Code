#ifndef _TM1629_H_
#define _TM1629_H_


#include "Common.h"

/*********************************************************************************/
#define TM16XX_DELAY_COUNT                           (10)  //延时时间10us
/*********************************************************************************/
/*                设置模式命令                           */
#define    TM1629_SET_DATA_COM                       (0x40) //设置数据命令
#define    TM1629_SET_DISPLAY_COM                    (0x80) //设置显示控制命令
#define    TM1629_SET_ADD_COM                        (0xC0) //设置地址命苦



/*                数据命令设置                           */
#define    TM1629_WRITE_DATA_DISPLAY                 (0x40) //写数据到显示寄存器
#define    TM1629_READ_DATA_BUTTON                   (0x42) //读键扫数据
#define    TM1629_ADD_AUTO_ADDRESS                   (0x40) //自动地址增加
#define    TM1629_ADD_FIXED_ADDRESS                  (0x44) //固定地址
#define    TM1629_SET_COMMON_MODE                    (0x40) //设置为普通模式



/*                显示控制命令设置                           */
#define    TM1629_DISPLAY_LUMINANCE_1                (0x80) //设置亮度等级为 1  Luminance
#define    TM1629_DISPLAY_LUMINANCE_2                (0x81) //设置亮度等级为 2  Luminance
#define    TM1629_DISPLAY_LUMINANCE_3                (0x82) //设置亮度等级为 3  Luminance
#define    TM1629_DISPLAY_LUMINANCE_4                (0x83) //设置亮度等级为 4  Luminance
#define    TM1629_DISPLAY_LUMINANCE_5                (0x84) //设置亮度等级为 5  Luminance
#define    TM1629_DISPLAY_LUMINANCE_6                (0x85) //设置亮度等级为 6  Luminance
#define    TM1629_DISPLAY_LUMINANCE_7                (0x86) //设置亮度等级为 7  Luminance
#define    TM1629_DISPLAY_LUMINANCE_8                (0x87) //设置亮度等级为 8  Luminance

#define    TM1629_DISPLAY_ON                         (0x80) //显示开关为关
#define    TM1629_DISPLAY_OFF                        (0x88) //显示开关为开
#define    TM16XX_CMD_DISPLAY_OFF                    (0x00)
#define    TM16XX_CMD_DISPLAY_ON                     (0x08)

/*                地址命令设置                           */
#define    TM1629_ADD_0                              (0xC0) //地址0

/*********************************************************************************/
typedef struct 
{   
    void(*pSetStbHigh)(void);
    void(*pSetStbLow)(void);     
    void(*pSetClkHigh)(void);
    void(*pSetClkLow)(void);
    void(*pSetDataHigh)(void);
    void(*pSetDataLow)(void);
    void(*pSetDataInput)(void);
    void(*pSetDataOutput)(void);
    uint8_t(*pGetDataGpio)(void);
    void(*pWait)(uint16_t u16Delay);    
}sTM16XX_HW_Typedef;

typedef struct 
{
	uint8_t *pu8Button;
    uint8_t *pu8Led;
    uint16_t *pu16Led;

	//uint8_t ReadKeyCycle;
	//uint8_t DisplayCycle;	
    
    sTM16XX_HW_Typedef HWPort;

}sTM16XX_Typedef;

//extern stTM1617_Typedef   stTM1617;


/*********************************************************************************/
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_WriteByte(sTM16XX_Typedef *psTM16XX,uint8_t u8WriteDisplayData);
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_WriteCmd(sTM16XX_Typedef *psTM16XX,uint8_t u8WriteCmd);
/*! \note      
 *  \param  
 *  \retval  
 *  \retval  
 */
uint8_t TM16XX_ReadByte(sTM16XX_Typedef *psTM16XX);
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_WriteDataAutoIncrease(sTM16XX_Typedef *psTM16XX);
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_WriteDataAutoIncreaseFixed(sTM16XX_Typedef *psTM16XX, uint8_t u8RegisterAddress);
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_ReadKey(sTM16XX_Typedef *psTM16XX);


/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_LEDControl(sTM16XX_Typedef *psTM16XX,uint8_t u8DisplayControlCmd);

#if 0
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void TM16XX_LEDControl(sTM16XX_Typedef *psTM16XX);
#endif





#endif



