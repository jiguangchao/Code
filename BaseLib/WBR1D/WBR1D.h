#ifndef _WBR1D_H_
#define _WBR1D_H_


#include "BSP_Include.h"
#include "BaseLib.h"

typedef struct
{

  uint8_t u8PMSADropsFlag;  //掉线标志
  uint8_t u8PMSADropscount; //掉线计数

  uint16_t u16CrcTest1;      //crc测试
  uint16_t u16CrcTest2;      //crc测试


  uint16_t u16ReadPeriod;
  uint32_t u32ReadWait;
  uint32_t u32Dormancywait;

  //uint8_t u8FailureState;

  uint16_t u16PM10Numerical;
  uint16_t u16PM25Numerical;
  uint16_t u16PM100Numerical;

  //USART_Function_Typedef  pUSART2;
  //Serial_Typedef  Frame;

} WBR1D_Typedef;
/* ----------------------- Defines ------------------------------------------*/
extern WBR1D_Typedef WBR1D;
extern USART_Function_Typedef WBR1D_UART;


/*********************************************************************
 * @fn      WBR1DUartInit
 * @brief   WBR1D模块串口初始化
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t WBR1DUartInit(USART_Function_Typedef *pUSART);
/**********************************************************************************************/
/*                                   串口0 运行初始化函数                                      */
/**********************************************************************************************/
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevInit(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevReset(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialReceiveOK(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevError(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialSendPrepair(Serial_Typedef *pSerial);
/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Receive_Interrupt_Parse(Serial_Typedef *pSerial, uint8_t u8Data);

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Receive_Interrupt(uint8_t u8Data);

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Send_Interrupt(uint8_t *pu8Data);
/*********************************************************************
 * @fn      WBR1D_Hearbeat_1
 * @brief   IOT模块心跳1
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Hearbeat_1(Serial_Typedef *pSerial);
/*********************************************************************
 * @fn      WBR1D_Hearbeat_2
 * @brief   IOT模块心跳2
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Hearbeat_2(Serial_Typedef *pSerial);  

/*********************************************************************
 * @fn      WBR1D_Test
 * @brief   IOT模块测试报文
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Test(Serial_Typedef *pSerial); 

#endif
