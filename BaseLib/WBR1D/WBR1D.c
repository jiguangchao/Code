/*******************************************************************
*
*    DESCRIPTION:
*
*    AUTHOR:
*
*    HISTORY:
*
*    DATE:
*
*******************************************************************/

//#include "BSP_Include.h"
#include "Common.h"
#include "BaseLib.h"

/** @addtogroup
  * @{
  */
/** @addtogroup
  * @{
  */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* An array of Modbus functions handlers which associates Modbus function
 * codes with implementing functions.
 */

uint8_t WBR1D_TX_Buff[WBR1D_TX_BUFF];
uint8_t WBR1D_RX_Buff[WBR1D_RX_BUFF];


WBR1D_Typedef WBR1D;
USART_Function_Typedef WBR1D_UART;

/*********************************************************************
 * @fn      WBR1DUartInit
 * @brief   WBR1D模块串口初始化
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t WBR1DUartInit(USART_Function_Typedef *pUSART)
{
    uint32_t u32Baud;
    uint8_t u8BaudID;
    uint8_t u8Stops;
    uint8_t u8Parity;

    u8BaudID       =      pUSART->Frame.u8BaudID;
    u8Stops        =      pUSART->Frame.u8Stop ;
    u8Parity       =      pUSART->Frame.u8Parity ;

    
    /*
    //pModbusSlave                   = pUSART;
    pUSART->Frame.eParity           = WBR1D_UART.Frame.eParity;
    pUSART->Frame.eStop             = WBR1D_UART.Frame.eStop ;
    pUSART->Frame.u8BaudID          = WBR1D_UART.Frame.eBaudID ;
    */
    pUSART->Frame.u8IdleNmsConfig   = u8BaudRateBytePeriodNmsConfigTab[u8BaudID];
    u32Baud                         = (uint32_t)eBaudRateTab[u8BaudID];

    pUSART->SerialDevInit               = BSP_UART0_SerialDevInit;
    pUSART->SerialDevReset              = BSP_UART0_SerialDevReset;
    pUSART->SerialReceiveOK             = BSP_UART0_SerialReceiveOK;
    pUSART->SerialDevError              = BSP_UART0_SerialDevError;
    pUSART->SerialSendPrepair           = BSP_UART0_SerialSendPrepair;
    
    
    pUSART->pStart                      = BSP_UART0_SendStart;
    pUSART->pStop                       = BSP_UART0_SendStop;
    pUSART->pSend                       = BSP_UART0_DirectSend;
    pUSART->pReceive                    = BSP_UART0_DirectReceive;
    pUSART->pDMARxConfig                = BSP_UART0_DMA_RxConfig;
    pUSART->pDMATxConfig                = BSP_UART0_DMA_TxConfig;
    pUSART->pDMATxSendStatus            = BSP_UART0_SendOK;
    pUSART->pGetBusBusy                 = BSP_UART0_GetBusBusy;

    pUSART->pReceive();
    pUSART->pDMARxConfig();
    pUSART->pDMATxConfig();


    pUSART->Frame.pu8Send                 = WBR1D_TX_Buff;
    pUSART->Frame.pu8Receive              = WBR1D_RX_Buff;
    //pUSART->Frame.u16SendBytesMax         = WBR1D_TX_BUFF;
    pUSART->Frame.u16ReceiveBuffMax       = WBR1D_RX_BUFF;
    
    URAT0_InterruptReceive                = UART_WBR1D_Receive_Interrupt;
    URAT0_InterruptSend                   = UART_WBR1D_Send_Interrupt;

    BSP_USART0_Init(u8BaudID,u8Stops,u8Parity);
    
    return OK;
}

/**********************************************************************************************/
/*                                   串口0 运行初始化函数                                      */
/**********************************************************************************************/
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevInit(Serial_Typedef *pSerial)
{
    return OK;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevReset(Serial_Typedef *pSerial)
{
    return OK;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialReceiveOK(Serial_Typedef *pSerial)
{
    return OK;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialDevError(Serial_Typedef *pSerial)
{
    return OK;
}
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART0_SerialSendPrepair(Serial_Typedef *pSerial)
{
    return OK;
}


/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Receive_Interrupt_Parse(Serial_Typedef *pSerial, uint8_t u8Data)
{
   if ( !pSerial->u8IdleNms )
    {   
        pSerial->u16ReceiveBytesCnt = 0;
    }
    /* Init Frame idle time */
    pSerial->u8IdleNms = pSerial->u8IdleNmsConfig;
    
    if (pSerial->u16ReceiveBytesCnt < pSerial->u16ReceiveBuffMax) 
    {
        pSerial->pu8Receive[pSerial->u16ReceiveBytesCnt] = u8Data; 
        switch (pSerial->u16ReceiveBytesCnt)
        {
        case 0:
            
            if ( 0x55 != pSerial->pu8Receive[0] )
            {
                return !OK;
            }
            pSerial->u16ReceiveBytesCnt++;
            break;

        case 1:

            if ( 0xaa != pSerial->pu8Receive[1] )
            {
                return !OK;
            }
            pSerial->u16ReceiveBytesCnt++;
            break;
        case 2:

            pSerial->u16ReceiveBytesCnt++;
            break;

        case 3:

            if (0x00 == pSerial->pu8Receive[3])
            {
                WBR1D_Hearbeat_1(&WBR1D_UART.Frame);
            }
            else if (0x01 == pSerial->pu8Receive[3])
            {
                WBR1D_Hearbeat_2(&WBR1D_UART.Frame);
            }

           pSerial->u16ReceiveBytesCnt++;
           break;

        case 4:
            pSerial->u16ReceiveBytesCnt++;
            break;

        case 5:    

            pSerial->u16ReceiveBytesMax = pSerial->pu8Receive[5] + 7;
            pSerial->u16ReceiveBytesCnt++;
            break;


        default:

            pSerial->u16ReceiveBytesCnt++;
            if ( pSerial->u16ReceiveBytesCnt >= pSerial->u16ReceiveBytesMax )
            {
                pSerial->u16ReceiveBytesCnt = 0;
                pSerial->eRxState = eSERIAL_BUS_STATE_RECEIVE_OK;
            }
            break;
        }   
        return OK;
    }
    return !OK;
}

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Receive_Interrupt(uint8_t u8Data)
{
    return UART_WBR1D_Receive_Interrupt_Parse(&WBR1D_UART.Frame,u8Data);
}

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_WBR1D_Send_Interrupt(uint8_t *pu8Data)
{
    return USART_GetSendbuff(&WBR1D_UART.Frame,pu8Data);
}

/*********************************************************************
 * @fn      WBR1D_Hearbeat_1
 * @brief   IOT模块心跳1
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Hearbeat_1(Serial_Typedef *pSerial) 
{   
    pSerial->pu8Send[0] = 0x55; 
    pSerial->pu8Send[1] = 0xaa;
    pSerial->pu8Send[2] = 0x03;
    pSerial->pu8Send[3] = 0x00;
    pSerial->pu8Send[4] = 0x00;
    pSerial->pu8Send[5] = 0x01;
    pSerial->pu8Send[6] = 0x00;
    pSerial->pu8Send[7] = 0x03;
    pSerial->u16SendBytesMax = 8;
    pSerial->u8RetransCnt = 1;
    USART_StateSetSend(&WBR1D_UART.Frame, WBR1D_UART.Frame.u16SendBytesMax, 0, 0);
}
/*********************************************************************
 * @fn      WBR1D_Hearbeat_2
 * @brief   IOT模块心跳2
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Hearbeat_2(Serial_Typedef *pSerial) 
{   
    pSerial->pu8Send[0] = 0x55; 
    pSerial->pu8Send[1] = 0xaa;
    pSerial->pu8Send[2] = 0x03;
    pSerial->pu8Send[3] = 0x00;
    pSerial->pu8Send[4] = 0x00;
    pSerial->pu8Send[5] = 0x01;
    pSerial->pu8Send[6] = 0x01;
    pSerial->pu8Send[7] = 0x04;
    pSerial->u16SendBytesMax = 8;
    pSerial->u8RetransCnt = 1;
    USART_StateSetSend(&WBR1D_UART.Frame, WBR1D_UART.Frame.u16SendBytesMax, 0, 0);
}


/*********************************************************************
 * @fn      WBR1D_Test
 * @brief   IOT模块测试报文
 * @param   无参数
 * @return  none
*********************************************************************/ 
void WBR1D_Test(Serial_Typedef *pSerial) 
{   
    pSerial->pu8Send[0] = 0x55; 
    pSerial->pu8Send[1] = 0xaa;
    pSerial->pu8Send[2] = 0x03;
    pSerial->pu8Send[3] = 0x01;
    pSerial->pu8Send[4] = 0x02;
    pSerial->pu8Send[5] = 0x03;
    pSerial->pu8Send[6] = 0x04;
    pSerial->pu8Send[7] = 0x05;
    pSerial->pu8Send[8] = 0x06;
    pSerial->pu8Send[9] = 0x07;
    pSerial->pu8Send[10] = 0x08;
    pSerial->pu8Send[11] = 0x09;
    pSerial->pu8Send[12] = 0x010;
    pSerial->pu8Send[13] = 0x011;

    pSerial->u16SendBytesMax = 14;
    pSerial->u8RetransCnt = 1;
    USART_StateSetSend(&WBR1D_UART.Frame, WBR1D_UART.Frame.u16SendBytesMax, 0, 0);
}