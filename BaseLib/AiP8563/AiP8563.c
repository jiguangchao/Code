

#include "Common.h"
#include "BaseLib.h"

/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteByte_Test(sAiP8563_Typedef *psAiP8563,uint8_t u8WriteDataAdd,uint8_t u8WriteData)
{


    I2C_start(&psAiP8563->I2C);

    I2C_send_byte(&psAiP8563->I2C, 0x88);

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_send_byte(&psAiP8563->I2C, u8WriteDataAdd);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_send_byte(&psAiP8563->I2C, u8WriteData);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    //I2C_no_ack(&psAiP8563->I2C);
    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_stop(&psAiP8563->I2C);
}

/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteByte(sAiP8563_Typedef *psAiP8563,uint8_t u8WriteDataAdd,uint8_t u8WriteData)
{


    I2C_start(&psAiP8563->I2C);

    I2C_send_byte(&psAiP8563->I2C, AIP8563_I2C_ADR_W);

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_send_byte(&psAiP8563->I2C, u8WriteDataAdd);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_send_byte(&psAiP8563->I2C, u8WriteData);

    //I2C_no_ack(&psAiP8563->I2C);
    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);

    I2C_stop(&psAiP8563->I2C);


}

/**
   * @brief
   * @param  
   * @retval
   */
void AiP8563_ReadMode(sAiP8563_Typedef *psAiP8563 , uint8_t u8addr )
{

    I2C_start(&psAiP8563->I2C);

    I2C_send_byte(&psAiP8563->I2C, AIP8563_I2C_ADR_W);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

    I2C_send_byte(&psAiP8563->I2C, u8addr);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }

}

/**
   * @brief
   * @param
   * @retval
   */
void AiP8563_ReadBytes(sAiP8563_Typedef *psAiP8563)
{
    uint8_t i;
    uint8_t u8Temp;
 
    I2C_start(&psAiP8563->I2C);

    I2C_send_byte(&psAiP8563->I2C, AIP8563_I2C_ADR_R);

    if (I2C_wait_ack(&psAiP8563->I2C))
    {
        return ;
    }
    psAiP8563->I2C.HwPort.pSetSdaInput();
	for (i = 0;i < 8;i++)
	{
        psAiP8563->I2C.HwPort.pSetSclHigh();
		psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);
		u8Temp <<= 1;
		u8Temp |= psAiP8563->I2C.HwPort.pGetSdaGpio();
		psAiP8563->I2C.HwPort.pSetSclLow();
		psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);
	}

    psAiP8563->I2C.HwPort.pSetSdaHigh();
    psAiP8563->I2C.HwPort.pSetSclHigh();
    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);
    psAiP8563->I2C.HwPort.pSetSclLow();
    psAiP8563->I2C.HwPort.pWait(AIP8563_DELAY_COUNT);
    psAiP8563->I2C.HwPort.pSetSdaOutput();

    psAiP8563->pu8ReadData[0] = u8Temp;

}

/*! \note  
 *  \param  写测试模式
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteSetTestMode(sAiP8563_Typedef *psAiP8563)
{
    uint8_t u8WriteCount;

    //AiP8563_WriteByte_Test( psAiP8563, 0x21,0x30 );
    
    AiP8563_WriteByte(psAiP8563, AIP8563_REGISTER_ADD_CONTROL_1,0x00 );//普通模式，时钟有效

    AiP8563_WriteByte(psAiP8563, AIP8563_REGISTER_ADD_CONTROL_2,0x00 );//设置 STOP = 0

    for ( u8WriteCount = 0; u8WriteCount < 7; u8WriteCount++)
    {
        AiP8563_WriteByte(psAiP8563, u8WriteCount + AIP8563_REGISTER_ADD_SECOND  , psAiP8563->pu8WriteData[u8WriteCount] );//写入时间值
    }
     

}

/*! \note  
 *  \param  读时间
 *  \retval  
 *  \retval  
 */
void AiP8563_ReadDate(sAiP8563_Typedef *psAiP8563 , uint8_t u8addr)
{
    AiP8563_ReadMode(psAiP8563,u8addr);

    AiP8563_ReadBytes(psAiP8563);

    switch ( u8addr )
    {
    case    AIP8563_REGISTER_ADD_CONTROL_1    :

        break;
    case    AIP8563_REGISTER_ADD_CONTROL_2    :

        break;

    case    AIP8563_REGISTER_ADD_SECOND       :  //秒寄存器地址    second
        psAiP8563->u8Second =  psAiP8563->pu8ReadData[0] & 0x7F ; 
        UART1_SendByte(psAiP8563->u8Second);
        break;

    case    AIP8563_REGISTER_ADD_MINUTE       :  //分寄存器地址    minute
        psAiP8563->u8Minute =  psAiP8563->pu8ReadData[0] & 0x7F ;
        UART1_SendByte(psAiP8563->u8Minute); 
        break;

    case    AIP8563_REGISTER_ADD_HOUR         :  //小时寄存器地址  hour
        psAiP8563->u8Hour =  psAiP8563->pu8ReadData[0] & 0x3F ; 
        UART1_SendByte(psAiP8563->u8Hour); 
        break;
    case    AIP8563_REGISTER_ADD_DATE         :  //日期寄存器地址  date
        psAiP8563->u8Date =  psAiP8563->pu8ReadData[0] & 0x3F ; 
        UART1_SendByte(psAiP8563->u8Date); 
        break;
    case    AIP8563_REGISTER_ADD_WEEK         :  //星期寄存器地址  week
        psAiP8563->u8Week =  psAiP8563->pu8ReadData[0] & 0x07 ; 
        UART1_SendByte(psAiP8563->u8Week); 
        break;
    case    AIP8563_REGISTER_ADD_MONTH        :  //月期寄存器地址  month
        psAiP8563->u8Month =  psAiP8563->pu8ReadData[0] & 0x1F ; 
        UART1_SendByte(psAiP8563->u8Month);
        break;
    case    AIP8563_REGISTER_ADD_YEAR         :  //年寄存器地址    year
        psAiP8563->u8Year =  psAiP8563->pu8ReadData[0]  ; 
        UART1_SendByte(psAiP8563->u8Year);
        break;
    case    AIP8563_REGISTER_ADD_MINUTE_ERROR :

        break;
    case    AIP8563_REGISTER_ADD_HOUR_ERROR   :

        break;
    case    AIP8563_REGISTER_ADD_DATE_ERROR   :

        break;
    case    AIP8563_REGISTER_ADD_WEEK_ERROR   :

        break;
    case    AIP8563_REGISTER_ADD_CLKOUT       :

        break;
    case    AIP8563_REGISTER_ADD_TIMER_CONTROL:

        break;
    case    AIP8563_REGISTER_ADD_TIMER_COUNT  :

        break;

    
    default:
        break;
    }

}



