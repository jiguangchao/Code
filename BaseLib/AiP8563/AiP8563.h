#ifndef _AIP8563_H_
#define _AIP8563_H_


#include "Common.h"



#define   AIP8563_DELAY_COUNT                           (10)  //延时时间10us

/*********************************************************************/
/*                             IIC地址                            */
/********************************************************************/
#define   AIP8563_I2C_ADR_W             (0xA2) //sensor I2C address + write bit
#define   AIP8563_I2C_ADR_R             (0xA3) //sensor I2C address + read bit
/*********************************************************************/
/*                             寄存器地址                            */
/********************************************************************/
#define   AIP8563_REGISTER_ADD_CONTROL_1                 (0x00)  //控制状态寄存器1地址 control state register
#define   AIP8563_REGISTER_ADD_CONTROL_2                 (0x01)  //控制状态寄存器2地址 control state register

#define   AIP8563_REGISTER_ADD_SECOND                    (0x02)  //秒寄存器地址    second
#define   AIP8563_REGISTER_ADD_MINUTE                    (0x03)  //分寄存器地址    minute
#define   AIP8563_REGISTER_ADD_HOUR                      (0x04)  //小时寄存器地址  hour
#define   AIP8563_REGISTER_ADD_DATE                      (0x05)  //日期寄存器地址  date
#define   AIP8563_REGISTER_ADD_WEEK                      (0x06)  //星期寄存器地址  week
#define   AIP8563_REGISTER_ADD_MONTH                     (0x07)  //月期寄存器地址  month
#define   AIP8563_REGISTER_ADD_YEAR                      (0x08)  //年寄存器地址    year

#define   AIP8563_REGISTER_ADD_MINUTE_ERROR              (0x09)  //分 报警 寄存器地址   minute
#define   AIP8563_REGISTER_ADD_HOUR_ERROR                (0x0A)  //时 报警 寄存器地址 hour
#define   AIP8563_REGISTER_ADD_DATE_ERROR                (0x0B)  //日 报警 寄存器地址  date
#define   AIP8563_REGISTER_ADD_WEEK_ERROR                (0x0C)  //星 报警 寄存器地址  week

#define   AIP8563_REGISTER_ADD_CLKOUT                    (0x0D)  //CLKOUT频率寄存器地址
#define   AIP8563_REGISTER_ADD_TIMER_CONTROL             (0x0E)  //定时器控制寄存器地址
#define   AIP8563_REGISTER_ADD_TIMER_COUNT               (0x0F)  //定时器倒计数数值寄存器地址



/****************************************************************************/
/*                                控制状态寄存器1                            */
/*****************************************************************************/

#define   AIP8563_CONTROL_1_SET_TESTMODE                 (0x80)  //设置测试模式

#define   AIP8563_CONTROL_1_SET_TIMESTOP                 (0x20)  //设置芯片时钟停止

#define   AIP8563_CONTROL_1_SET_RESET                    (0x08)  //电源复位有效

/****************************************************************************/
/*                               CLKOUT频率寄存器                            */
/*****************************************************************************/

#define   AIP8563_CLKOUT_SET_DISABLE                    (0x00) //禁用CLKOUT

/****************************************************************************/
/*                               定时器控制寄存器                           */
/*****************************************************************************/

#define   AIP8563_TIMER_CONTROL_SET_DISABLE             (0x00)  //禁用定时器



/*******************************************************************************************/
typedef struct 
{
    uint8_t  u8Second;
    uint8_t  u8Minute;
    uint8_t  u8Hour;
    uint8_t  u8Date;
    uint8_t  u8Week;
    uint8_t  u8Month;
    uint8_t  u8Year;

    uint8_t *pu8ReadData;
    uint8_t *pu8WriteData;
    
    I2C_Bus_Typedef I2C;

} sAiP8563_Typedef;
/*******************************************************************************************/
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteByte_Test(sAiP8563_Typedef *psAiP8563,uint8_t u8WriteDataAdd,uint8_t u8WriteData);
/*! \note  
 *  \param  
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteByte(sAiP8563_Typedef *psAiP8563,uint8_t u8WriteDataAdd,uint8_t u8WriteData);
/**
   * @brief
   * @param  
   * @retval
   */
void AiP8563_ReadMode(sAiP8563_Typedef *psAiP8563 , uint8_t u8addr );
/**
   * @brief
   * @param
   * @retval
   */
void AiP8563_ReadBytes(sAiP8563_Typedef *psAiP8563);
/*! \note  
 *  \param  写测试模式
 *  \retval  
 *  \retval  
 */
void AiP8563_WriteSetTestMode(sAiP8563_Typedef *psAiP8563);
/*! \note  
 *  \param  读时间
 *  \retval  
 *  \retval  
 */
void AiP8563_ReadDate(sAiP8563_Typedef *psAiP8563 , uint8_t u8addr);


#endif

