#ifndef _NEXSYS_H_
#define _NEXSYS_H_


#include "BSP_Include.h"
#include "BaseLib.h"

typedef struct
{

  uint8_t u8PMSADropsFlag;  //掉线标志
  uint8_t u8PMSADropscount; //掉线计数

  uint16_t u16CrcTest1;      //crc测试
  uint16_t u16CrcTest2;      //crc测试


  uint16_t u16ReadPeriod;
  uint32_t u32ReadWait;
  uint32_t u32Dormancywait;

  //uint8_t u8FailureState;

  uint16_t u16PM10Numerical;
  uint16_t u16PM25Numerical;
  uint16_t u16PM100Numerical;

  //USART_Function_Typedef  pUSART2;
  //Serial_Typedef  Frame;

} NEXSYS_Typedef;
/* ----------------------- Defines ------------------------------------------*/
extern NEXSYS_Typedef NEXSYS;
extern USART_Function_Typedef NEXSYS_UART;


/*********************************************************************
 * @fn      NEXSYSUartInit
 * @brief   NEXSYS模块串口初始化
 * @param   无参数
 * @return  none
*********************************************************************/
uint8_t NEXSYSUartInit(USART_Function_Typedef *pUSART);
/**********************************************************************************************/
/*                                   串口0 运行初始化函数                                      */
/**********************************************************************************************/
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SerialDevInit(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SerialDevReset(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SerialReceiveOK(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SerialDevError(Serial_Typedef *pSerial);
/**
  * @brief
  * @param
  * @retval None
  */
uint8_t BSP_UART1_SerialSendPrepair(Serial_Typedef *pSerial);
/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_NEXSYS_Receive_Interrupt_Parse(Serial_Typedef *pSerial, uint8_t u8Data);

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_NEXSYS_Receive_Interrupt(uint8_t u8Data);

/**
 * @brief  
 * @param
 * @retval None
 */
uint8_t UART_NEXSYS_Send_Interrupt(uint8_t *pu8Data);
/*********************************************************************
 * @fn      NEXSYS_Hearbeat_1
 * @brief   IOT模块心跳1
 * @param   无参数
 * @return  none
*********************************************************************/ 
void NEXSYS_Hearbeat_1(Serial_Typedef *pSerial);
/*********************************************************************
 * @fn      NEXSYS_Hearbeat_2
 * @brief   IOT模块心跳2
 * @param   无参数
 * @return  none
*********************************************************************/ 
void NEXSYS_Hearbeat_2(Serial_Typedef *pSerial);  

/*********************************************************************
 * @fn      NEXSYS_Test
 * @brief   IOT模块测试报文
 * @param   无参数
 * @return  none
*********************************************************************/ 
void NEXSYS_Test(Serial_Typedef *pSerial); 

#endif
